# xsarsea

cmod, detrend, and others sar processing tools over ocean.

# Instalation

xsar_sea need [xsar](https://gitlab.ifremer.fr/sarlib/saroumane/-/blob/develop/README.md)  instalation

now you can install xsarsea

```conda activate xsar```

for user:
```
pip install git+https://gitlab.ifremer.fr/sarlib/xsarsea.git
```

for developement:
```
git clone https://gitlab.ifremer.fr/sarlib/xsarsea.git
cd xsarsea
pip install -r requirements.txt
pip install -e .
```
