#############
API reference
#############

sigma0_detrend
==============

.. automodule:: xsarsea
    :members: sigma0_detrend

streaks_direction
=================

.. automodule:: xsarsea.streaks
    :members: streaks_direction