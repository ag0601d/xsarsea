import logging
import sys
import xarray
import os
from scipy import signal
import numpy as np
import cross_spectra_core
def process_RAR_WV_SLC(one_tiff):
    slc = cross_spectra_core.read_slc(one_tiff)
    dn_s = np.power(abs(slc['digital_number'].sel({'pol':'VV'})),2)
    mean_azi = dn_s.mean(dim="azimuth")
    theta = slc['incidence'].mean(dim=['azimuth','range'])
    mean_azi_ground = mean_azi/np.sin(np.radians(theta))
    signal_log = np.log(mean_azi_ground)
    z = np.polyfit(signal_log.range, signal_log, 3)
    p = np.poly1d(z)
    detrended = mean_azi_ground/p(signal_log.range)
    fs = np.pi*2*float(np.sin(np.radians(theta)))/(float(slc.attrs['rangeSpacing']))#*(np.pi*2)
    nperseg = 1024
    f, Pxx_den = signal.welch(detrended, fs, nperseg=nperseg)
    newds = xarray.Dataset()
    for kk in slc.attrs:
        newds.attrs[kk] = slc.attrs[kk]
    newds['pur_range_spectrum'] = xarray.DataArray(Pxx_den,coords={"kx":2*np.pi*f},dims=['kx'],attrs={
            'nperseg':nperseg,
            'fs':fs,
            'unit':'??',
            'description':'digital numbers averaged on azimuth dim, slant-->ground detrended FFT using Welsh method'}
                                                  )
    output = '/home1/scratch/agrouaze/test_L1S_WV_pur_range_spectra/RAR_spec_%s'%(os.path.basename(one_tiff).replace('.tiff','.nc'))

    if not os.path.exists(os.path.dirname(output)):
        os.makedirs(os.path.dirname(output))
    newds.to_netcdf(output)
    logging.info('output nc : %s',output)
    return newds
