# coding: utf-8
"""
A Grouazel
March 2022
"""
import numpy as np
import logging
import xarray
import conversion_polar_cartesian
from cross_spectra_core import read_slc,compute_SAR_cross_spectrum
import spectrum_clockwise_to_trigo
import spectrum_rotation
#from display_one_spectra import reference_oswK_1145m_60pts
reference_oswK_1145m_60pts = np.array([0.005235988, 0.00557381, 0.005933429, 0.00631625, 0.00672377,
    0.007157583, 0.007619386, 0.008110984, 0.008634299, 0.009191379,
    0.0097844, 0.01041568, 0.0110877, 0.01180307, 0.01256459, 0.01337525,
    0.01423822, 0.01515686, 0.01613477, 0.01717577, 0.01828394, 0.01946361,
    0.02071939, 0.02205619, 0.02347924, 0.02499411, 0.02660671, 0.02832336,
    0.03015076, 0.03209607, 0.03416689, 0.03637131, 0.03871796, 0.04121602,
    0.04387525, 0.04670605, 0.0497195, 0.05292737, 0.05634221, 0.05997737,
    0.06384707, 0.06796645, 0.0723516, 0.07701967, 0.08198893, 0.08727881,
    0.09290998, 0.09890447, 0.1052857, 0.1120787, 0.1193099, 0.1270077,
    0.1352022, 0.1439253, 0.1532113, 0.1630964, 0.1736193, 0.1848211,
    0.1967456, 0.2094395])

def get_phi_rel2satelliteazimuth(phi_relative_2_north, headingplatform_relative_to_north):
    """
    args:
        phi_relative_2_north (float): vector to support wav directions clockwise in degree relative to the North
        headingplatform_relative_to_north (float): platform heading angle clockwise in degree relative to the North
    return:
       phi_rel2satelliteazimuth (float): angle clockwise in degree relative to the heading of the platform
    """
    logging.debug('headingplatform_relative_to_north : %s', headingplatform_relative_to_north)
    phi_rel2satelliteazimuth = phi_relative_2_north + (360. - headingplatform_relative_to_north)
    phi_rel2satelliteazimuth = phi_rel2satelliteazimuth % 360
    return phi_rel2satelliteazimuth


def compute_MACS_profile(spectra, phi_relative_2_north, headingplatform_relative_to_north):
    """
    :param spectra: (2D np.ndarray)  could be sp_re or sp_im with coords oswK and oswPhi relative to North) 72x60 (phi,k)
    :param phi_relative_2_north: (1D np.ndarray) vector to support wave directions clockwise in degree relative to the North
    :param headingplatform_relative_to_north: (1D np.ndarray) platform heading angle clockwise in degree relative to the North

    :return:
    """
    # step 0: go for k phi dimension order
    spectra = spectra.T
    logging.debug('spectra shape %s', spectra.shape)
    # step 1: change phi coordinates of the spectra from North reference to heading reference
    logging.debug('phi_relative_2_north = %s', phi_relative_2_north)
    phi_rel2satelliteazimuth = get_phi_rel2satelliteazimuth(phi_relative_2_north, headingplatform_relative_to_north)
    logging.debug('phi_rel2satelliteazimuth = %s', phi_rel2satelliteazimuth)
    # i get sorted index to have the transformation to apply on the spectra
    idxsort = np.argsort(phi_rel2satelliteazimuth)
    logging.debug('idxsort = %s', idxsort)
    logging.debug('spectra shape = %s', spectra.shape)
    ind = np.where(abs(phi_rel2satelliteazimuth - 90) == np.amin(abs(phi_rel2satelliteazimuth - 90)))[0][0]
    logging.debug('ind of phi==90 : %s %s', ind, phi_rel2satelliteazimuth[ind])
    macs_vect = np.nanmean(spectra[:, ind - 1:ind + 2], axis=1)
    return macs_vect, phi_rel2satelliteazimuth


def get_macs_float(spectra, phi_relative_2_north, headingplatform_relative_to_north, oswK):
    """
    impro agrouaze pour retrouver fig4 du papier JGR de Huimin (avec un filtrage sur les wavelength en 15m et 20m)
    :param spectra: (2D np.ndarray)  could be sp_re or sp_im with coords oswK and oswPhi relative to North) 72x60 (phi,k)
    :param phi_relative_2_north: (1D np.ndarray) vector to support wave directions clockwise in degree relative to the North
    :param headingplatform_relative_to_north: (1D np.ndarray) platform heading angle clockwise in degree relative to the North
    :param oswK: (1D np.ndarray) wave number vector [m.^-1]
    :return:
        macs: (float) mean of the MACS profile
    """
    macs_vect, _ = compute_MACS_profile(spectra, phi_relative_2_north, headingplatform_relative_to_north)
    minlimk = (2 * np.pi / 30)
    maxlimk = (2 * np.pi / 15)
    logging.debug('oswK = %s', oswK)
    logging.debug('minlimk %s maxlimk %s' % (minlimk, maxlimk))
    filtkeep = (minlimk <= oswK) & (oswK <= maxlimk)
    logging.debug('filtkeep oswK :%s %s %s', filtkeep.sum(), len(filtkeep), filtkeep)
    macs_vect_filt = macs_vect[filtkeep]
    macs = np.mean(macs_vect_filt)
    return macs


def prepare_xspectra(tiff_full_path,tauval=2,part='Im'):
    ds_slc = read_slc(tiff_full_path, slice_subdomain=None, resolution=None, resampling=None)
    allspecs, frange, fazimuth, allspecs_per_sub_domain, splitting_image, limits_sub_images = compute_SAR_cross_spectrum(
        ds_slc,
        N_look=3, look_width=0.25, look_overlap=0., look_window=None, range_spacing=None,
        welsh_window='hanning', nperseg={'range': 512, 'azimuth': 512},
        noverlap={'range': 256, 'azimuth': 256},
        spacing_tol=1e-3, debug_plot=False, merge_along_tau=True, return_periodoXspec=False)
    if part == 'Im':
        # thecmap = 'PuOr'
        if isinstance(allspecs, xarray.DataArray):
            coS = allspecs
        else:
            coS = allspecs['cross-spectrum_%stau' % tauval].mean(
                dim='%stau' % tauval).imag  # co spectrum = imag part of cross-spectrum
    else:
        # thecmap = cmap
        coS = abs(allspecs['cross-spectrum_%stau' % tauval].mean(
            dim='%stau' % tauval).real)  # co spectrum = real part of cross-spectrum
    # new_spec_Polar = xsarsea.conversion_polar_cartesian.from_xCartesianSpectrum(coS,Nphi=72,
    #                                                                             ksampling='log',
    #                                                                 **{'Nk' : 60,
    #                                                                    'kmin' :
    #                                                                        reference_oswK_1145m_60pts[
    #                                                                            0],
    #                                                                    'kmax' :
    #                                                                        reference_oswK_1145m_60pts[
    #                                                                            -1]})
    new_spec_Polar = conversion_polar_cartesian.from_xCartesianSpectrum(coS, Nphi=72,
                                                                        ksampling='log',
                                                                        **{'k': reference_oswK_1145m_60pts})
    # new_spec_Polar = xsarsea.conversion_polar_cartesian.from_xCartesianSpectrum(coS,Nphi=72,
    #                                                                             ksampling='log',
    #                                                                             **{'k' : reference_oswK_1145m_60pts})
    # new_spec_Polar.assign_coords({'k':reference_oswK_1145m_60pts}) # test agrouaze

    heading = ds_slc.heading
    crossSpectraPol = new_spec_Polar.squeeze()
    crossSpectraPol = spectrum_clockwise_to_trigo.apply_clockwise_to_trigo(crossSpectraPol)
    crossSpectraPol = spectrum_rotation.apply_rotation(crossSpectraPol,
                                                         -90.)  # This is for having origin at North # 1dec21 change +90 to -90 on a case descending
    crossSpectraPol = spectrum_rotation.apply_rotation(crossSpectraPol, heading)
    crossSpectraPol.attrs['heading'] = heading
    return crossSpectraPol

def get_profile_MACS_from_xspec_slc(tiff_full_path,tauval=2,part='Im',crossSpectraPol=None):
    """

    :param handler: (netCDF4 Dataset handler)
    :param spectra_var_name: (str) example 'oswQualityCrossSpectraIm','oswPolSpec','oswQualityCrossSpectraRe'
    :param spectra (2D nd.array)
    :return:
    """
    if crossSpectraPol is None:
        crossSpectraPol = prepare_xspectra(tiff_full_path, tauval=tauval, part=part)
    phi_relative_2_north = np.sort(np.degrees(crossSpectraPol.phi)%360)
    heading = crossSpectraPol.heading
    macs_vect, phi_rel2satelliteazimuth = compute_MACS_profile(spectra=crossSpectraPol,
                                                               phi_relative_2_north=phi_relative_2_north,
                                                               headingplatform_relative_to_north=heading)
    return macs_vect


