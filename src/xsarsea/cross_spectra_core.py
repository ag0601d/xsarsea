#!/usr/bin/env python 
import numpy as np
import numexpr as ne
import xarray as xr
import time
import sys
import os
import logging
import pdb
import scipy
from scipy.signal import fftconvolve
import matplotlib
import matplotlib.pyplot as plt
import xrft.xrft as xrft
import xsar
import xarray
from scipy import signal
import copy
import pyfftw
import PIL
# sys.path.append('/home1/datahome/fnouguie/research/numeric/R3S/SAR/')
# from postprocessing import ground_regularization
def get_imagette_indice(onetiff,wv_slc_meta):

    good_indice = None
    # find the indice of the tiff
    imagette_number = os.path.basename(onetiff).split('-')[-1].replace('.tiff','')
    logging.info('imagette_number : %s',imagette_number)
    for ddi,ddname in enumerate(wv_slc_meta.subdatasets) :
        if 'WV_'+imagette_number in ddname :
            logging.info("matching ddname : %s",ddname)
            good_indice = ddi
    logging.info('indice in meta sentinel1 gdal driver is : %s',good_indice)
    return good_indice

from numba import jit
@jit(nopython=True,parallel=True)
def supermean(a):
    return np.mean(a)

def read_annotations(annotation_file_xml):
    """
    it should be done in xsar but it is not currently exactly the values expected
    :return:
    """
    logging.info('read annotation in xsarsea.cross_spectra_core')
    # Finding pixel spacing
    import xml.etree.ElementTree as ET
    #info = 'annotation/s1a-wv1-slc-vv-20191005t165023-20191005t165026-029326-035559-045.xml'
    tree = ET.parse(annotation_file_xml)
    root = tree.getroot()
    rangeSpacing = float(root.find('imageAnnotation').find('imageInformation').find('rangePixelSpacing').text)
    azimuthSpacing = float(root.find('imageAnnotation').find('imageInformation').find('azimuthPixelSpacing').text)

    # Finding mean incidence angle in the vignette
    mean_inc = float(root.find('imageAnnotation').find('imageInformation').find('incidenceAngleMidSwath').text)

    # Defining range and azimuth axis
    #meta_dict = {PIL.TiffTags.TAGS[key] : img.tag[key] for key in img.tag_v2.keys()}
    return rangeSpacing,azimuthSpacing,mean_inc

def read_slc(onetiff,slice_subdomain=None,resolution=None,resampling=None):
    """

    :param one_wv: SAFE (str) full path without the last /
    :param slice_subdomain : slice objet to define the sub part of imagette to perform cross spectra
    :param resolution: dict for instance for 200m {'atrack' : int(np.round(200 / sar_meta.pixel_atrack_m)), 'xtrack': int(np.round(200 / sar_meta.pixel_xtrack_m))}
    :param rasterio.enums.Resampling.rms for instance
    :return:
    """
    logging.info('tiff: %s',onetiff)
    logging.info('resampling : %s',resampling)
    safepath = os.path.dirname(os.path.dirname(onetiff))
    logging.info('safepath: %s',safepath)
    tmp = xsar.get_test_file(safepath)
    logging.info('tmp: %s',tmp)
    wv_slc_meta = xsar.Sentinel1Meta(tmp)
    logging.debug('wv_slc_meta %s',wv_slc_meta)
    good_indice = get_imagette_indice(onetiff,wv_slc_meta)
    if resolution is not None:
        if resolution['atrack']==1 and resolution['xtrack']==1:
            #June 2021, a patch because currently resolution 1:1 for image and rasterio returns an error
            slc = xsar.open_dataset(wv_slc_meta.subdatasets[good_indice],resolution=None,resampling=None)
        else:
            slc = xsar.open_dataset(wv_slc_meta.subdatasets[good_indice],resolution=resolution,resampling=resampling)
    else:
        slc = xsar.open_dataset(wv_slc_meta.subdatasets[good_indice])
    logging.info('max xtrack val : %s',slc['xtrack'].values[-1])
    slc = slc.rename({'atrack' : 'azimuth','xtrack' : 'range'})
    annotation_file_xml = onetiff.replace('measurement',"annotation").replace('.tiff','.xml')
    #rangeSpacing,azimuthSpacing,mean_inc = read_annotations(annotation_file_xml) # I prefere to use the result from xsar since this atribute is update with a specific resolution given
    rangeSpacing = slc.attrs['pixel_xtrack_m']
    azimuthSpacing= slc.attrs['pixel_atrack_m']
    mean_inc = np.mean(slc['incidence']).values
    platform_heading = slc.attrs['platform_heading']
    logging.debug('slant rangeSpacing %s',rangeSpacing)
    #changement des coordinates range and azimuth like Nouguier
    raxis = np.arange(len(slc['range']))
    aaxis = np.arange(len(slc['azimuth']))
    logging.debug('mean incidence : %s',mean_inc)
    logging.debug('factor to turn into ground range: %s',rangeSpacing / np.sin(np.radians(mean_inc)))
    # 2 dec2021: reader xsar gives ground range spacing since oct 2021 so there is no need to apply the division by the sin(theta) below
    raxis = raxis * rangeSpacing #/ np.sin(np.radians(mean_inc)) #test agrouaze 18 oct 2021
    aaxis = aaxis * azimuthSpacing
    logging.info('range coords before : %s',slc['range'].values)
    #slc = slc.assign_coords({'range':raxis,'azimuth':aaxis}) # I turn off the coords change performed by Noug because I think it is already done in xsar (not sure but it can explain the problem of grid I have)
    slc = slc.assign_coords({'range' : raxis,'azimuth' : aaxis}) # I put back the coords change because otherwise I cannot get the energy pattern expected like Nouguier
    logging.info('max range val : %s',raxis[-1])
    logging.info('range coords after : %s',slc['range'].values)
    # on attend du 4.1m environ pour le ground range spacing! dixit doc ESA, Nouguier et annotations (en slant)
    slc.attrs.update({'azimuthSpacing' : azimuthSpacing,'rangeGroundSpacing' : rangeSpacing,
                       'heading' : platform_heading})
    # test agrouaze 18 oct 21
    # slc.attrs.update({'azimuthSpacing' : azimuthSpacing,'rangeSpacing' : rangeSpacing,
    #                    'heading' : platform_heading})
    # compute modulation
    #slc['modulation'] = slc['digital_number']/np.mean(abs(slc['digital_number'].values))#*0.0000000001 - 1000000. #division by 1000 for test
    slc['modulation'] = slc['digital_number']
    logging.info('max modulation : %s',np.amax(slc['modulation'].values))
    logging.info('mean modulation : %s',np.mean(slc['modulation'].values))
    logging.info('max digital_number : %s',np.amax(slc['digital_number'].values))
    if slice_subdomain is not None:
        logging.info('subset in the image: %s',slice_subdomain)
        slc = slc.isel(range=slice_subdomain['range'],azimuth=slice_subdomain['azimuth'],pol=0)
    return slc


def ground_regularization(ds, *,method='nearest', **kwargs):
    """
    pcopy paste from /home1/datahome/fnouguie/research/numeric/R3S/SAR/postprocessing.py (april 2021)
    Compute ground regularization: Interpolation of ds variables on a regular ground grid.

    Args:
        ds (xarray): xarray on (slant_range, azimuth coordinates)

    Keyword Args:
        method (str): method of interpolation (nearest seems to give the best results)
        range_spacing (float, optional): ground spacing [meter]. If not defined, the mean of projected ranges steps is used. You should provide the NOT padded ground range resolution

    Returns:
        (xarray): same as ds but interpolated on (range, azimuth) coordinates
    """
    import warnings
    warnings.warn('Update slant to ground projection with sinc interpolation.')
    logging.debug('type ds: %s',type(ds))
    #tmp = xr.DataArray(np.array([50000]),dims='altitude',coords={'altitude':np.arange(1)}) #added agrouaze for test


    #ds = ds.rename({'xtrack':'slant_range'})
    #ds = ds.rename({'atrack' : 'azimuth'})
    #
    #ds['altitude'] = tmp*
    #ds.attrs['altitude'] = 0.5
    logging.info('ds.coords : %s',ds.coords)
    if 'range' in ds.coords:
        print('Ground regularization: range coordinates already in SLC object. SLC is assumed to be on ground range coordinates already.')
        return ds
    if 'slant_range' not in ds.coords:
        raise ValueError('Please provide ds with slant_range coordinates')

    if 'altitude' in kwargs:
        altitude = kwargs.pop('altitude')
    elif 'altitude' in ds.attrs:
        altitude = ds.altitude
    else:
        raise ValueError('Please provide range spacing value or altitude in kwargs')
    logging.debug('all finite slant ranges? %s',np.isfinite(ds['slant_range'].values).all())
    logging.debug('min slant %s',ds['slant_range'].values.min())
    ground_ranges = np.sqrt(ds['slant_range']**2-altitude**2).rename('ground_range')
    logging.debug('all finite ground ranges? %s',np.isfinite(ground_ranges.values).all())
    logging.debug('ground_ranges !: %s',ground_ranges)
    range_spacing = kwargs.pop('range_spacing', np.diff(ground_ranges).mean().data)
    logging.debug('range spaceing : %s',range_spacing)
    range = np.arange(ground_ranges.min(), ground_ranges.max(), range_spacing)
    logging.debug('range : %s \n %s all ifinite? %s',range.shape,range,np.isfinite(range).all())
    range = xr.DataArray(range, dims=('range',), coords={'range':range})
    tmptmp = ds.assign_coords(slant_range=ground_ranges).interp(**{'slant_range':range},
                                                              method=method)
    logging.debug('tmptmp: %s',tmptmp)
    #return tmptmp.dropna(dim='range').drop('slant_range')
    return tmptmp.drop('slant_range')

def compute_SAR_cross_spectrum(slc,*, N_look=3, look_width=0.25, look_overlap=0., look_window=None, range_spacing=None,
                welsh_window='hanning', nperseg={'range':512, 'azimuth':512}, noverlap={'range':256, 'azimuth':256},
                               spacing_tol=1e-3,debug_plot=False,merge_along_tau=True,return_periodoXspec=True, **kwargs):
    """
    Compute SAR cross spectrum using a 2D Welch method. Looks are centered on the mean Doppler frequency
    If ds contains only one cycle, spectrum wavenumbers are added as coordinates in returned DataSet, othrewise, they are passed as variables (k_range, k_azimuth).
    
    Args:
        slc (xarray): SAR Single Look Complex image. Output of sar_processor()
    
    Keyword Args:
        mode (str): focus or unfocus
        N_look (int): Number of looks
        look_width (float): Percent of the total bandwidth used for a single look in [0,1]
        look_overlap (float): Percent of look overlaping [0,1]. Negative values means space between two looks
        look_window (xarray): window used in look processing
        ds (xarray): R3S simulation or SLC
        merge_along_tau (bool) : default is True -> final dimension should be like this  (0tau: 48, 1tau: 32, 2tau: 16, freq_azimuth: 1024, freq_range: 1024, pol: 1) if False -> merge tau per sub region
        return_periodoXspec (bool): default is True -> list of the X spectra (Im,Re 0,1,2 tau) returned for each sub images
        kwargs (dict): other arguments passed to cowelch2d()
        
    Returns:
        (xarray): SAR NRCS spectrum
    """
    
    if N_look<1:raise ValueError('N_look must be greater than 0')
    if np.abs(look_width)>=1:raise ValueError('look_width must be in [0,1] range')
    #logging.debug('mean of modulation on the wole image %s',np.mean(slc['modulation'].values))
    with xr.set_options(keep_attrs=True):
        gslc = ground_regularization(slc, range_spacing = range_spacing, **kwargs)
    
#     Np = gslc.sizes['azimuth']
    
#     nlook = int(look_width*Np)
#     nlookoverlap = int(np.rint(look_overlap*look_width*Np)) # This noverlap is different from the noverlap of welch_kwargs
    logging.debug('gslc : %s',gslc)
    if isinstance(welsh_window, str) or type(welsh_window) is tuple:
        winx = scipy.signal.get_window(welsh_window, nperseg['range'])
        winy = scipy.signal.get_window(welsh_window, nperseg['azimuth'])
        win = np.outer(winx, winy)
    else:
        win = np.asarray(welsh_window)
        if len(win.shape) != 2:
            raise ValueError('window must be 2-D')
        if win.shape[0] > x.shape[-2]:
            raise ValueError('window is longer than x.')
        if win.shape[1] > x.shape[-1]:
            raise ValueError('window is longer than y.')
        nperseg['range'] = win.shape[-2]
        nperseg['azimuth'] = win.shape[-1]

    
    stepra = nperseg['range'] - noverlap['range']
    stepaz = nperseg['azimuth'] - noverlap['azimuth']
    logging.debug('gslc.sizes : %s %s',type(gslc.sizes),gslc.sizes)
    logging.debug('nperseg %s %s',type(nperseg),nperseg)
    indicesx = np.arange(0, gslc.sizes['range']-nperseg['range']+1, stepra)
    indicesy = np.arange(0, gslc.sizes['azimuth']-nperseg['azimuth']+1, stepaz)
    logging.debug('indicesx : %s',indicesx)
    logging.debug('indicesy : %s',indicesy)
    dir = np.diff(gslc['range'].data)
    dia = np.diff(gslc['azimuth'].data)
    if not (np.allclose(dir, dir[0], rtol=spacing_tol) and np.allclose(dia, dia[0], rtol=spacing_tol)):
        raise ValueError("Can't take Fourier transform because coordinate is not evenly spaced")
    
    frange = np.fft.fftfreq(nperseg['range'], dir[0]/(2*np.pi))
    fazimuth = np.fft.fftfreq(nperseg['azimuth'], dia[0]/(2*np.pi))    
    xspecs = list()
    variable_name = 'digital_number'
    #variable_name = 'modulation'
    logging.debug('variable_name : %s',variable_name)
    cpt=0
    splitting_image ={}
    # TODO cette partie peut etre gerer avec dask!! calcul des xspectre sur les sous domaine
    start_range_s = []
    stop_range_s = []
    start_azi_s = []
    stop_azi_s = []
    gslc[variable_name] = gslc[variable_name].persist() # added by agrouaze speed the computation
    for kx, indx in enumerate(indicesx):
        for ky, indy in enumerate(indicesy):
            t0 = time.time()
            plot = True if ((kx==0) and (ky==0)) else False
            sliceors = {'range':slice(indx, indx+nperseg['range']), 'azimuth':slice(indy, indy+nperseg['azimuth'])}
            start_range_s.append(sliceors['range'].start)
            stop_range_s.append(sliceors['range'].stop)
            start_azi_s.append(sliceors['azimuth'].start)
            stop_azi_s.append(sliceors['azimuth'].stop)
            sub = gslc[sliceors]
            axis_id_range = sub[variable_name].get_axis_num(dim='range')
            logging.debug('axis_id_range = %s',axis_id_range)
            logging.debug('sub au debut : %s',np.mean(sub[variable_name].values))
            splitting_image[cpt] = sliceors
            logging.debug('nb not finite in s0 %s/%s ',(np.isfinite(sub[variable_name].values)==False).sum(),
                         sub[variable_name].values.size)
            #s
            if debug_plot:
                plt.figure()
                plt.title('real %s shape %s'%(variable_name,sub[variable_name].values.squeeze().shape))
                plt.pcolor(sub[variable_name].values.squeeze().real,cmap='jet')
                plt.colorbar()
                plt.show()

                plt.figure()
                plt.title('before norm')
                plt.plot(sub[variable_name].values.squeeze().real[10,:],'.-')
                plt.xlabel('range')
                plt.show()
                one_profile_1 = sub[variable_name].values.squeeze().real[10,:]
                one_profile_1_along_azimuth=  sub[variable_name].values.squeeze().real[:,10]
            #sub = sub[variable_name].values

            logging.debug('sub after detrend: %s max: %s',np.mean(sub[variable_name].values),np.amax(sub[variable_name].values))


            #sub = sub[variable_name]/np.abs(sub[variable_name]).mean(dim=['range', 'azimuth']) # proposition Nouguier 17 may to do normalization before /detrend

            tmpmean = supermean(np.abs(sub[variable_name]).values) # agrouaze test with numba to speed up
            sub = sub[variable_name] /tmpmean
            # logging.info('test numexpr : %s %s',type(sub[variable_name].values),sub[variable_name].values.dtype)
            # tmpabs_re = ne.evaluate('abs(aa)',local_dict={'aa':sub[variable_name].values}).real
            # tmpabs_1j = ne.evaluate('abs(aa)',local_dict={'aa':sub[variable_name].values}).imag
            # tmpabs = tmpabs_re+1j*tmpabs_1j
            # logging.info('tmpabs : %s %s %s',tmpabs.dtype,type(tmpabs),tmpabs.shape)
            # tmpmean = ne.evaluate('sum(bb)',local_dict={'bb':tmpabs})/tmpabs.size
            # sub = sub[variable_name]/tmpmean
            #             sub = sub*win # already commented in the original Nouguier code
            if debug_plot and False:
                plt.figure()
                plt.title('real %s shape %s after normalization' % (variable_name,sub.values.squeeze().shape))
                plt.pcolor(sub.values.squeeze().real,cmap='jet')
                plt.colorbar()
                plt.show()
                one_profile_2 = sub.values.squeeze().real[10,:]
                one_profile_2_along_azimuth = sub.values.squeeze().real[:,10]
            #################### DETREND ###########################
            sub = scipy.signal.detrend(sub,axis_id_range)
            if debug_plot and False:
                plt.figure()
                plt.title('real %s shape %s after detrend' % (variable_name,sub.shape))
                plt.pcolor(sub.squeeze().real,cmap='jet')
                plt.colorbar()
                plt.show()
                one_profile_3 = sub.squeeze().real[10,:]
                one_profile_3_along_azimuth = sub.squeeze().real[:,10]

                plt.figure(figsize=(30,6))
                plt.title('profiles along range dim')
                plt.grid(True)
                #plt.plot(one_profile_1,'.-',label='raw real signal in sub image')
                plt.plot(one_profile_2,'.-',label='normalized real signal in sub image')
                plt.plot(one_profile_3,'.--',label='norm+detrend real signal in sub image')
                plt.legend()
                plt.show()

                plt.figure(figsize=(30,6))
                plt.title('profiles along azimuth dim')
                plt.grid(True)
                #plt.plot(one_profile_1,'.-',label='raw real signal in sub image')
                plt.plot(one_profile_2_along_azimuth,'.-',label='normalized real signal in sub image')
                plt.plot(one_profile_3_along_azimuth,'.--',label='norm+detrend real signal in sub image')
                plt.legend()
                plt.show()
            #sub = sub/np.mean(np.mean(np.abs(sub),axis=2),axis=1) # for test I remove this normalization agrouaze


            logging.debug('sub max : %s',np.amax(sub))
            logging.debug('sub mean val : %s real %s imag %s squeeze %s',np.mean(sub),np.mean(sub.real),np.mean(sub.imag),np.mean(sub.squeeze()))
            if len(sub.shape)==3:
                subxr = xr.DataArray(sub,dims=['pol','azimuth','range'])
            else:
                subxr = xr.DataArray(sub,dims=['azimuth','range'])
            t1 = time.time()
            tmplooks = compute_looks(subxr, N_look=N_look, look_width=look_width, look_overlap=look_overlap,
                                        look_window=look_window, plot=plot,debug_plot=debug_plot)
            if cpt % 20 == 0:
                logging.info('sub %s %s %s/%s time compute_looks %1.3f sec time total to treat the sub image %1.3f sec',
                             type(sub),subxr.shape,cpt,len(indicesx)*len(indicesy),time.time()-t1,time.time()-t0)
            #ajout agrouaze pour ajouter une coordonnee de portion dimage
            for taux in tmplooks.keys():
                for lookid,look in enumerate(tmplooks[taux]):
                    tmplooks[taux][lookid] = look.assign_coords({'portionImageid':cpt})
                    if debug_plot and lookid==0:
                        plt.figure()
                        plt.grid(True)
                        plt.title('look %s tau %s'%(lookid,taux))
                        plt.contourf(tmplooks[taux][lookid].squeeze())
                        plt.colorbar()
                        plt.show()
                    logging.debug('look tmp: %s',type(tmplooks[taux][lookid]))

                    # ajout pour avoir les limite du domaine dans limage
                    # tmplooks[taux][lookid]['start_range'] = xarray.DataArray(np.array([sliceors['range'].start]),
                    #                                                          dims=['slicedim'],coords={'slicedim':[0]})
                    # tmplooks[taux][lookid]['stop_range'] = xarray.DataArray(np.array([sliceors['range'].stop]),
                    #                                                          dims=['slicedim'],
                    #                                                          coords={'slicedim' : [0]})
                    # tmplooks[taux][lookid]['start_azimuth'] = xarray.DataArray(np.array([sliceors['azimuth'].start]),
                    #                                                          dims=['slicedim'],
                    #                                                          coords={'slicedim' : [0]})
                    # tmplooks[taux][lookid]['stop_azimuth'] = xarray.DataArray(np.array([sliceors['azimuth'].stop]),
                    #                                                          dims=['slicedim'],
                    #                                                          coords={'slicedim' : [0]})
            xspecs.append(tmplooks)
            cpt += 1
#     return xspecs

    allspecs = list()
    logging.info('N looks: %s',N_look)
    #add allspecs per subdomain agrouaze
    allspecs_per_sub_domain = {}
    for subdomain in range(cpt):
        logging.debug('subdomain %s',subdomain)

        listxspecPerDomain = xspecs[subdomain]
        logging.debug('listxspecPerDomain : %s',listxspecPerDomain.keys())
        all_spectra_one_domain = []
        for tau in range(N_look) :
            l = []
            for item in listxspecPerDomain[str(tau) + 'tau']:
                    l.append(item.drop_vars(['freq_range','freq_azimuth']))
            l = xr.concat(l,dim=str(tau) + 'tau').rename('cross-spectrum_' + str(tau) + 'tau')
            all_spectra_one_domain.append(l)
        if return_periodoXspec:
            all_spectra_one_domain = xr.merge(all_spectra_one_domain,compat='override')
            all_spectra_one_domain = all_spectra_one_domain.assign_coords(freq_range=np.fft.fftshift(frange))
            all_spectra_one_domain = all_spectra_one_domain.assign_coords(freq_azimuth=np.fft.fftshift(fazimuth))
            all_spectra_one_domain = all_spectra_one_domain.rename(freq_range='kx',freq_azimuth='ky')
            allspecs_per_sub_domain[subdomain] = all_spectra_one_domain

    #add the limits of the sub images
    limits_sub_images = xarray.Dataset()
    dim_name_sub_dom = 'subdomDim'
    limits_sub_images['start_range_s'] = xarray.DataArray(start_range_s,dims=dim_name_sub_dom,
                                                                coords={dim_name_sub_dom:np.arange(cpt)})
    limits_sub_images['stop_range_s'] = xarray.DataArray(stop_range_s,dims=dim_name_sub_dom,
                                                                coords={dim_name_sub_dom : np.arange(cpt)})
    limits_sub_images['start_azi_s'] = xarray.DataArray(start_azi_s,dims=dim_name_sub_dom,
                                                                coords={dim_name_sub_dom : np.arange(cpt)})
    limits_sub_images['stop_azi_s'] = xarray.DataArray(stop_azi_s,dims=dim_name_sub_dom,
                                                                coords={dim_name_sub_dom : np.arange(cpt)})
    #code nouguier original
    for tau in range(N_look):
        l = [item.drop_vars(['freq_range','freq_azimuth']) for sublist in xspecs for item in sublist[str(tau)+'tau']]
        #logging.debug('l tau : %s',l)
        l = xr.concat(l, dim=str(tau)+'tau').rename('cross-spectrum_'+str(tau)+'tau')
        allspecs.append(l)
    allspecs = xr.merge(allspecs, compat='override')
    allspecs = allspecs.assign_coords(freq_range=np.fft.fftshift(frange))
    allspecs = allspecs.assign_coords(freq_azimuth=np.fft.fftshift(fazimuth))
    allspecs = allspecs.rename(freq_range='kx',freq_azimuth='ky')
    return allspecs,frange,fazimuth,allspecs_per_sub_domain,splitting_image,limits_sub_images


def compute_looks(gslc,*, N_look=3, look_width=0.25, look_overlap=0., look_window=None, plot=False,debug_plot=False):
    """
    """
    true_ampl = True #(True for exp1 D1 dataset generation)since F Nouguier true fft amplitude or not doesn t really matter because for x spectra computation we add so many windows
    # that it is difficult to say what energy levels represent at the end. But this assumption is considered as wrong
    # by ESA since many geophysical parameters are derived from the X-spectra and a specific MTF.
    
    Np = gslc.sizes['azimuth']
    nlook = int(look_width*Np) # number of pulse in a look
    noverlap = int(np.rint(look_overlap*look_width*Np)) # This noverlap is different from the noverlap of welch_kwargs
    dim_range = 'range'
    mydop = xrft.dft(gslc, dim=['azimuth'], detrend=None, window=None, shift=False,true_amplitude=true_ampl,true_phase=False)
    logging.debug('mydop : %s',mydop)
    if debug_plot:
        vmin = np.amin(mydop.squeeze().real.values)
        vmax = np.amax(mydop.squeeze().real.values)
        levels = np.linspace(vmin,vmax,50)
        plt.figure()
        plt.title('dft true amplitude\n max : %s min:%s' % (vmax,vmin))
        plt.contourf(mydop.squeeze().real,levels=levels)
        plt.colorbar()
        plt.show()
    logging.debug('gslc mean : %s',np.mean(gslc.values))
    logging.debug('mydop mean %s %s',mydop.shape,np.mean(mydop))
    logging.debug('mydop[freq_azimuth] %s',mydop['freq_azimuth'].values)
    mean_dop = np.float((np.abs(mydop).mean(dim=dim_range)*mydop['freq_azimuth']).mean()/(np.abs(mydop).mean(dim=dim_range)).mean())
    logging.debug('mean dop numpy: %s',mean_dop)
    ic = np.argmin(np.abs(mydop['freq_azimuth'].sortby('freq_azimuth').data-mean_dop)) # index of the mean doppler (on the reordered frequency axis)
    logging.debug('ic numpy: %s',ic)
    
    step = nlook-noverlap
    indices = np.arange(0, Np-nlook+1, step)
    indices = np.concatenate((-np.flipud(indices)[:-1],indices))
    indices+=np.int(ic)
    indices = indices-int(nlook/2) if N_look%2 else indices-int(noverlap/2)
    indices = indices[np.where(np.logical_and(indices>0,indices<Np-nlook+1))]
    indices = np.sort(indices[np.argsort(np.abs(indices+int(nlook/2)-np.int(ic)))[0:N_look]])
    
    if look_window is None:
        win = 1.
    elif isinstance(look_window, str) or type(look_window) is tuple:
        win = scipy.signal.get_window(look_window, nlook)
    else:
        win = np.asarray(look_window)
        if len(win.shape) != 1:
            raise ValueError('window must be 1-D')
        if len(win)> Np:
            raise ValueError('window is longer than x.')
        nlook = win.shape
    if plot:plt.figure()
    looks_spec = list()
    # mylooks=list()
    for ind in indices:
        mywin = xr.DataArray(np.zeros(Np), dims=('freq_azimuth',), coords={'freq_azimuth':mydop['freq_azimuth'].sortby('freq_azimuth')})
        mywin[{'freq_azimuth':slice(ind, ind+nlook)}]=win
        if plot:mywin.plot()
        #  mywin = mywin.reindex_like(mydop) # This line is useless because it is automatically done when multiplying with mydop (xarray capabilities !)
        logging.debug('mydop x mywin mean %s dtype: %s',np.mean(mydop*mywin),(mydop*mywin).dtype)
        look = xrft.idft(mydop*mywin, true_phase=False, dim=['freq_azimuth'], detrend=None, window=False, #true hase= True is test from agrouaze
                         shift=True,true_amplitude=true_ampl)
        if debug_plot:
            plt.figure()
            plt.title('look : ind = %s'%ind)
            plt.contourf(look)
            plt.colorbar()
            plt.show()
        logging.debug('look mean values :%s min real aprt: %s',np.mean(look.values),look.values.real.min())
        if debug_plot:
            plt.figure()
            plt.title('look #%s after idft (retour dans le domain spatial)'%ind)
            plt.pcolor(look.squeeze().real)
            plt.colorbar()
            plt.show()

        # mylooks.append(np.abs(look)**2)
        logging.debug('np.abs(look)**2 %s %s',(np.abs(look)**2).dtype,(np.abs(look)**2).shape)
        logging.debug('toto : min %s max %s',(np.abs(look) ** 2).values.min(),(np.abs(look) ** 2).values.max())
        tmp_looki_spec = xrft.dft(np.abs(look)**2, dim=['range','azimuth'], detrend='linear',true_amplitude=true_ampl)
        logging.debug('tmp_looki_spec min real : %s',tmp_looki_spec.values.real.min())
        logging.debug('tmp_looki_spec : %s',tmp_looki_spec)
        if debug_plot:
            plt.figure()
            plt.title('look spectra final #%s after idft (retour dans le domain freq)' % ind)
            plt.pcolor(tmp_looki_spec.squeeze().real)
            plt.colorbar()
            plt.show()
        looks_spec.append(tmp_looki_spec)
        
    if plot:(np.abs(mydop).mean(dim=dim_range)/np.abs(mydop).mean(dim=dim_range).max()).plot()

    looks_spec = xr.concat(looks_spec, dim='look')
    xspecs = dict()
    for l1 in range(N_look):
        for l2 in range(l1, N_look):
            xspec = looks_spec[{'look':l1}]*np.conj(looks_spec[{'look':l2}])
            if debug_plot:
                plt.figure()
                plt.title('final Xspectra look  %s versus %s' % (l1,l2))
                plt.pcolor(xspec.squeeze().real)
                plt.colorbar()
                plt.show()
                jmlksmdlkqsm
            #  xspec = np.conj(looks_spec[{'look':l1}])*looks_spec[{'look':l2}]
            if str(l2-l1)+'tau' in xspecs.keys():
                xspecs[str(l2-l1)+'tau'].append(xspec)
            else:
                xspecs[str(l2-l1)+'tau']=[xspec]       
    return xspecs



# def freq_comp(N, delta_x, real, shift):
#     # agrouaze copy / paste from xrft.py
#     # calculate frequencies from coordinates
#     # coordinates are always loaded eagerly, so we use numpy
#     if real is None:
#         fftfreq = [np.fft.fftfreq] * len(N)
#     else:
#         # Discard negative frequencies from transform along last axis to be
#         # consistent with np.fft.rfftn
#         fftfreq = [np.fft.fftfreq] * (len(N) - 1)
#         fftfreq.append(np.fft.rfftfreq)
#
#     k = [fftfreq(Nx, dx) for (fftfreq, Nx, dx) in zip(fftfreq, N, delta_x)]
#
#     if shift:
#         k = [np.fft.fftshift(l) for l in k]
#
#     return k


def compute_looks_pyfftw ( gslc,*,N_look=3,look_width=0.25,look_overlap=0.,look_window=None,plot=False,debug_plot=False ) :
    """
    wisdom : plan for pyfftw (pre computed)
    """
    true_ampl = True

    Np = gslc.sizes['azimuth']
    nlook = int(look_width * Np)  # number of pulse in a look
    noverlap = int(
        np.rint(look_overlap * look_width * Np))  # This noverlap is different from the noverlap of welch_kwargs

    dim_range = 'range'
    N,M = gslc.shape
    pyfftw.import_wisdom(gslc.attrs['wisdom_FFTW'])
    infft = pyfftw.empty_aligned((N,M),dtype='complex128')
    infft_c64= pyfftw.empty_aligned((N,M),dtype='complex64')
    infft_f64 = pyfftw.empty_aligned((N,M),dtype='float64')
    #infft_f64 = pyfftw.empty_aligned((M,N),dtype='float64')
    infft_trans = pyfftw.empty_aligned((N,M),dtype='complex128')
    outfft = pyfftw.empty_aligned((N,M),dtype='complex128')
    outfft_c64 = pyfftw.empty_aligned((N,M),dtype='complex64')
    outfft_trans = pyfftw.empty_aligned((N,M//2+1),dtype='complex128')
    outfft_trans = pyfftw.empty_aligned((N,M ),dtype='complex128')
    logging.info('outfft_trans.shape[-1] %s',outfft_trans.shape[-1])
    logging.info('infft_f64.shape[-1]//2 + 1 : %s',infft_f64.shape[-1]//2 + 1)
    logging.info('infft_f64 : %s',infft_f64.shape)
    #assert outfft_trans.shape[-1] == infft_f64.shape[-1]//2 + 1
    #assert outfft_trans.shape[0] == infft_f64.shape[0]
    #outfft_f64 = pyfftw.empty_aligned((N,M),dtype=float)
    outifft = pyfftw.empty_aligned((N,M),dtype='complex128')
    flag_FFTW = tuple([gslc.flag_FFTW] if type(gslc.flag_FFTW) == str else gslc.flag_FFTW)
    logging.debug('flag_FFTW : %s',flag_FFTW)
    myfft2 = pyfftw.FFTW(infft_c64, outfft_c64,axes=(0,), flags=flag_FFTW, direction='FFTW_FORWARD', threads=gslc.threads_FFTW)
    logging.info('gslc.values : %s',gslc.values.dtype)
    myfft2.update_arrays(gslc.values,outfft_c64)
    myfft2()
    mydop_values = outfft_c64.copy()
    if true_ampl:
        mydop_values = mydop_values * gslc.azimuthSpacing
    logging.debug('my dop with pyfftw : %s %s',type(mydop_values),mydop_values.shape)
    dia = [gslc.azimuthSpacing]
    logging.debug('dia: %s',dia)
    freq_az = np.fft.fftfreq(N,dia[0] )
    freq_ra = np.fft.fftfreq(M,gslc.rangeSpacing)
    logging.debug('freq_az %s %s %s',len(freq_az),freq_az.shape,freq_az)
    #freq_ra = np.fft.fftfreq(M,gslc.rangeSpacing)
    mydop = xarray.DataArray(mydop_values,
                             dims=['freq_azimuth','range'],
                             coords={
                                    'freq_azimuth':freq_az,
                                     'range':gslc['range'],
                                     })
    # fin de la premiere evolution xrft -> pyfftw (validee avec True_amplitude = True)
    logging.debug('mydop : %s',mydop.shape)
    if debug_plot :
        vmin = np.amin(mydop.squeeze().real.values)
        vmax = np.amax(mydop.squeeze().real.values)
        levels = np.linspace(vmin,vmax,50)
        plt.figure()
        plt.title('dft true amplitude\n max : %s min:%s' % (vmax,vmin))
        plt.contourf(mydop.squeeze().real,levels=levels)
        plt.colorbar()
        plt.show()
    logging.debug('gslc mean : %s',np.mean(gslc.values))
    logging.debug('mydop mean %s %s',mydop.shape,np.mean(mydop))
    mean_dop = np.float(
        (np.abs(mydop).mean(dim=dim_range) * mydop['freq_azimuth']).mean() / (np.abs(mydop).mean(dim=dim_range)).mean())
    logging.debug('mean dop : %s',mean_dop)
    ic = np.argmin(np.abs(mydop['freq_azimuth'].sortby(
        'freq_azimuth').data - mean_dop))  # index of the mean doppler (on the reordered frequency axis)
    logging.debug('ic: %s',ic)
    step = nlook - noverlap
    indices = np.arange(0,Np - nlook + 1,step)
    indices = np.concatenate((-np.flipud(indices)[:-1],indices))
    indices += np.int(ic)
    indices = indices - int(nlook / 2) if N_look % 2 else indices - int(noverlap / 2)
    indices = indices[np.where(np.logical_and(indices > 0,indices < Np - nlook + 1))]
    indices = np.sort(indices[np.argsort(np.abs(indices + int(nlook / 2) - np.int(ic)))[0 :N_look]])

    if look_window is None :
        win = 1.
    elif isinstance(look_window,str) or type(look_window) is tuple :
        win = scipy.signal.get_window(look_window,nlook)
    else :
        win = np.asarray(look_window)
        if len(win.shape) != 1 :
            raise ValueError('window must be 1-D')
        if len(win) > Np :
            raise ValueError('window is longer than x.')
        nlook = win.shape
    if plot : plt.figure()
    looks_spec = list()
    # mylooks=list()
    for ind in indices :
        mywin = xr.DataArray(np.zeros(Np),dims=('freq_azimuth',),
                             coords={'freq_azimuth' : mydop['freq_azimuth'].sortby('freq_azimuth')})
        mywin[{'freq_azimuth' : slice(ind,ind + nlook)}] = win
        if plot : mywin.plot()
        #  mywin = mywin.reindex_like(mydop) # This line is useless because it is automatically done when multiplying with mydop (xarray capabilities !)
        # look = xrft.idft(mydop * mywin,true_phase=False,dim=['freq_azimuth'],detrend=None,window=False,
        #                  # true hase= True is test from agrouaze
        #                  shift=True,true_amplitude=true_ampl)
        # second pyfftw call
        logging.info('2nd call pyfftw type input : %s',(mydop * mywin).values.dtype)
        myifft2 = pyfftw.FFTW(infft,outfft,axes=(0,),flags=flag_FFTW,direction='FFTW_BACKWARD',threads=gslc.threads_FFTW)
        logging.debug('mydop x mywin mean %s dtype: %s',np.mean(mydop * mywin),(mydop*mywin).dtype)

        myifft2.update_arrays((mydop * mywin).values,outifft)
        myifft2()
        look_values = outifft.copy()
        logging.debug('look_values : %s raw min real part %s',look_values.shape,look_values.real.min())

        if true_ampl:

            #pass
            logging.debug('true ampl fac: %s',np.diff(freq_az)[0])
            #look_values = look_values*np.diff(freq_az)[0]
            look_values = look_values /(gslc.azimuthSpacing)
            look_values = np.fft.ifftshift(look_values)

            #look_values = 2*np.pi*look_values/(np.diff(freq_az)[0])
        # plt.figure()
        # plt.contourf(look_values)
        # plt.colorbar()
        # plt.show()
        look = xarray.DataArray(look_values,
                            dims=['azimuth','range'],
                             coords={
                                    'azimuth':gslc['azimuth'],
                                    'range':gslc['range']
                                     })
        logging.debug('look mean values :%s min real  : %s',np.mean(look.values),look.values.real.min())
        # fin 2nd pyfftw call dev/test validated 10th sept 2021
        if debug_plot :
            plt.figure()
            plt.title('look #%s after idft (retour dans le domain spatial)' % ind)
            plt.pcolor(look.squeeze().real)
            plt.colorbar()
            plt.show()

        # mylooks.append(np.abs(look)**2)
        #tmp_looki_spec = xrft.dft(np.abs(look) ** 2,dim=[dim_range,'azimuth'],detrend='linear',true_amplitude=true_ampl)
        logging.info('infft_f64 : %s',infft_f64.shape)
        logging.info('outfft_trans : %s',outfft_trans.shape)
        logging.info('abs(look)2 :%s %s',(np.abs(look) ** 2).dtype,(np.abs(look) ** 2).shape)
        #fft_builders = pyfftw.builders.fft((np.abs(look) ** 2).values,threads=gslc.threads_FFTW)
        #logging.info('fft_builders : %s %s %s %s %s %s',fft_builders, dir(fft_builders),fft_builders.input_shape,fft_builders.input_dtype,
        #             fft_builders.output_shape,fft_builders.output_dtype)

        myfft2_v2 = pyfftw.FFTW(infft_trans,outfft_trans,axes=(0,1),flags=flag_FFTW,direction='FFTW_FORWARD',threads=gslc.threads_FFTW)
        logging.info('toto : min %s max %s',(np.abs(look) ** 2).values.min(),(np.abs(look) ** 2).values.max())
        toto = (np.abs(look) ** 2).values.astype('complex128')

        #toto = np.fft.fftshift(toto,1)
        myfft2_v2.update_arrays(toto,outfft_trans)
        #outfft_trans = np.fft.fftshift(outfft_trans,1)
        #outfft_trans = fft_builders()
        if true_ampl :
            outfft_trans = outfft_trans * gslc.azimuthSpacing
        #detrend (ie minus the mean plan)
        outfft_trans_real = signal.detrend(outfft_trans.real)
        outfft_trans_imag = signal.detrend(outfft_trans.imag)
        outfft_trans = outfft_trans_real+1j*outfft_trans_imag

        logging.info('outfft_trans : %s %s min real :%s',outfft_trans.shape,outfft_trans.dtype,outfft_trans.real.min())
        tmp_looki_spec_values = outfft_trans.copy()
        tmp_looki_spec = xarray.DataArray(tmp_looki_spec_values,
                            dims=['freq_azimuth','freq_range'],
                            #dims=['azimuth','range'],
                             coords={
                                    #'azimuth':gslc['azimuth'],
                                    'freq_azimuth':freq_az,
                                    'freq_range':freq_ra,
                                    #'range':gslc['range']
                                     })
        if debug_plot :
            plt.figure()
            plt.title('look spectra final #%s after idft (retour dans le domain freq)' % ind)
            plt.pcolor(tmp_looki_spec.squeeze().real)
            plt.colorbar()
            plt.show()
        looks_spec.append(tmp_looki_spec)

    if plot : (np.abs(mydop).mean(dim=dim_range) / np.abs(mydop).mean(dim=dim_range).max()).plot()

    looks_spec = xr.concat(looks_spec,dim='look')
    xspecs = dict()
    for l1 in range(N_look) :
        for l2 in range(l1,N_look) :
            xspec = looks_spec[{'look' : l1}] * np.conj(looks_spec[{'look' : l2}])
            if debug_plot :
                plt.figure()
                plt.title('final Xspectra look  %s versus %s' % (l1,l2))
                plt.pcolor(xspec.squeeze().real)
                plt.colorbar()
                plt.show()
            #  xspec = np.conj(looks_spec[{'look':l1}])*looks_spec[{'look':l2}]
            if str(l2 - l1) + 'tau' in xspecs.keys() :
                xspecs[str(l2 - l1) + 'tau'].append(xspec)
            else :
                xspecs[str(l2 - l1) + 'tau'] = [xspec]
    return xspecs