"""
author Antoine Grouazel
creation April 2022

"""
import xrft
import numpy as np
import logging
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit


def fit_gauss_factor(x, a, l):
    return a * np.exp(-(np.pi * x / l) ** 2)  # expression of the fit in the ATBD

def fit_gauss(x,l):
    return np.exp(-(np.pi*x/l)**2) # expression of the fit in the ATBD

def get_cut_off_profile(cross_spectrum,display_cutoff=False):
    """

    Parameters
    ----------
    cross_spectrum: xarray.Dataset with coordinates kx and ky (tau dimension removed by averaging)

    Returns
    -------

    """
    coV = xrft.idft(cross_spectrum, dim=('kx', 'ky'), shift=True)  # 2D ifft on the cartesian cross-spectrum
    coV.data = np.fft.fftshift(coV.data)
    coV = coV.assign_coords(freq_kx=2 * np.pi * coV.freq_kx.data, freq_ky=2 * np.pi * coV.freq_ky.data)
    coV = coV.rename(freq_kx='x', freq_ky='y')
    coVI = np.imag(coV).mean(dim='x')
    coV = np.real(coV).mean(dim='x')

    coVfit = coV.where(np.abs(coV.y) < 100, drop=True)  # I use only [-100 , 100] to do the fit. Can be a bit adjusted



    p, r = curve_fit(fit_gauss_factor, coVfit.y, coVfit.data,
                     p0=[450, 227])  # Here is the actual fitting which works pretty well
    if display_cutoff:
        plt.figure(figsize=(15, 6))
        coV.plot(marker='+')
        coVI.plot(marker='+')
        plt.plot(coV.y, fit_gauss_factor(coV.y, *p))
        plt.grid()
        plt.xlim([-400, 400])
    logging.info('cutt-off : {} m'.format(p[1]))

    # second may to estimate the cutoff
    coV /= coV.max()
    coVfit = coV.where(np.abs(coV.y) < 200, drop=True)  # I use only [-100 , 100] to do the fit. Can be a bit adjusted
    p, r = curve_fit(fit_gauss, coVfit.y, coVfit.data, p0=[200])
    if display_cutoff:
        plt.figure(figsize=(15, 6))
        coV.plot(marker='+')
        coVI.plot(marker='+')
        plt.plot(coV.y, fit_gauss(coV.y, *p))
        plt.grid()
        plt.xlim([-400, 400])
    cutoff = p[0]
    logging.info('cutt-off : {} m'.format(cutoff))
    return coV,cutoff
