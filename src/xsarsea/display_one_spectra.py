import sys
import time
import os
import logging
from matplotlib import pyplot as plt
from matplotlib import colors as mcolors
import xarray
cmap = mcolors.LinearSegmentedColormap.from_list("", ["white","violet","mediumpurple","cyan","springgreen","yellow","red"])
import numpy as np
sys.path.append('/home1/datahome/agrouaze/git/xsarseafork/src/')
sys.path.append('/home1/datahome/agrouaze/git/xsarseafork/src/xsarsea/')
#import xsarsea.conversion_polar_cartesian
import xsarsea
print('xsarsea',xsarsea.__file__)
import conversion_polar_cartesian
print('conversion_polar_cartesian',conversion_polar_cartesian.__file__)
import spectrum_clockwise_to_trigo
import display_xspectra_grid
import spectrum_rotation
reference_oswK_1145m_60pts = np.array([0.005235988, 0.00557381, 0.005933429, 0.00631625, 0.00672377,
    0.007157583, 0.007619386, 0.008110984, 0.008634299, 0.009191379,
    0.0097844, 0.01041568, 0.0110877, 0.01180307, 0.01256459, 0.01337525,
    0.01423822, 0.01515686, 0.01613477, 0.01717577, 0.01828394, 0.01946361,
    0.02071939, 0.02205619, 0.02347924, 0.02499411, 0.02660671, 0.02832336,
    0.03015076, 0.03209607, 0.03416689, 0.03637131, 0.03871796, 0.04121602,
    0.04387525, 0.04670605, 0.0497195, 0.05292737, 0.05634221, 0.05997737,
    0.06384707, 0.06796645, 0.0723516, 0.07701967, 0.08198893, 0.08727881,
    0.09290998, 0.09890447, 0.1052857, 0.1120787, 0.1193099, 0.1270077,
    0.1352022, 0.1439253, 0.1532113, 0.1630964, 0.1736193, 0.1848211,
    0.1967456, 0.2094395])

def core_plot_part(crossSpectraRePol,heading,fig=None,ax=None,thecmap=None,limit_display_wv=100,title='x spec',tauval=2):
    # partie display plot
    if fig is None :
        plt.figure(figsize=(20,8),dpi=100)
    if ax is None :
        ax = plt.subplot(1,1,1,polar=True)

        ax.set_theta_direction(-1)  # theta increasing clockwise
        ax.set_theta_zero_location("N")
    crossSpectraRe_redpol = crossSpectraRePol.where(np.abs(crossSpectraRePol.k) <= 2 * np.pi / limit_display_wv,
                                                    drop=True)

    # shift_phi = np.radians(heading-90)

    crossSpectraRe_redpol_phi_new = crossSpectraRe_redpol

    # crossSpectraRe_redpol_phi_new = crossSpectraRe_redpol.assign_coords({'phi':(crossSpectraRe_redpol.phi.values+shift_phi)[::-1]})
    # crossSpectraRe_redpol_phi_new = crossSpectraRe_redpol_phi_new/45000.
    crossSpectraRe_redpol_phi_new.plot(cmap=thecmap,alpha=0.8)
    #plt.pcolor(crossSpectraRe_redpol_phi_new.phi,crossSpectraRe_redpol_phi_new.k,crossSpectraRe_redpol_phi_new,cmap=thecmap)
    plt.grid(True)
    plt.plot(np.radians([heading,heading]),(0.01,max(crossSpectraRe_redpol.k)),'r-')
    plt.plot(np.radians([heading + 180,heading + 180]),(0.01,max(crossSpectraRe_redpol.k)),'r-')
    plt.plot(np.radians([heading + 90,heading + 90]),(0.01,max(crossSpectraRe_redpol.k)),'r-')
    plt.plot(np.radians([heading + 270,heading + 270]),(0.01,max(crossSpectraRe_redpol.k)),'r-')
    ax.text(np.radians(heading),(0.65 + (np.cos(np.radians(heading)) < 0) * 0.25) * max(crossSpectraRe_redpol.k),
            ' Azimuth',size=18,color='red',rotation=-heading + 90 + (np.sin(np.radians(heading)) < 0) * 180,
            ha='center')
    ax.text(np.radians(heading + 90),0.80 * max(crossSpectraRe_redpol.k),' Range',size=18,color='red',
            rotation=-heading + (np.cos(np.radians(heading)) < 0) * 180,ha='center')
    display_xspectra_grid.circle_plot(ax,r=[100,200,400])
    ax.set_rmax(2.0 * np.pi / limit_display_wv)
    ax.set_rmin(0) #security!!
    if title is None :
        plt.title('tau : %s' % tauval,fontsize=18)
    else :
        plt.title(title,fontsize=18)


def display_polar_spectra(allspecs,heading,part='Re',limit_display_wv=100,title=None,outputfile=None,
                          fig=None,ax=None,interactive=False,tau_id=2):
    """

    :param allspecs:
    :param heading:
    :param part:
    :param limit_display_wv:
    :param title:
    :param outputfile:
    :param fig:
    :param ax:
    :param interactive:
    :param tau_id:
    :return:
    """

    for tauval in [tau_id] :
        if part=='Im':
            thecmap='PuOr'
            if isinstance(allspecs,xarray.DataArray):
                coS = allspecs
            else:
                coS = allspecs['cross-spectrum_%stau' % tauval].mean(
                    dim='%stau' % tauval).imag  # co spectrum = imag part of cross-spectrum
        else:
            thecmap=cmap
            coS = abs(allspecs['cross-spectrum_%stau' % tauval].mean(
                dim='%stau' % tauval).real)  # co spectrum = real part of cross-spectrum
        # new_spec_Polar = xsarsea.conversion_polar_cartesian.from_xCartesianSpectrum(coS,Nphi=72,
        #                                                                             ksampling='log',
        #                                                                 **{'Nk' : 60,
        #                                                                    'kmin' :
        #                                                                        reference_oswK_1145m_60pts[
        #                                                                            0],
        #                                                                    'kmax' :
        #                                                                        reference_oswK_1145m_60pts[
        #                                                                            -1]})
        new_spec_Polar = conversion_polar_cartesian.from_xCartesianSpectrum(coS,Nphi=72,
                                                                                    ksampling='log',
                                                                                    **{'k' : reference_oswK_1145m_60pts})
        # new_spec_Polar = xsarsea.conversion_polar_cartesian.from_xCartesianSpectrum(coS,Nphi=72,
        #                                                                             ksampling='log',
        #                                                                             **{'k' : reference_oswK_1145m_60pts})
        #new_spec_Polar.assign_coords({'k':reference_oswK_1145m_60pts}) # test agrouaze

        crossSpectraRePol = new_spec_Polar.squeeze()
        crossSpectraRePol = spectrum_clockwise_to_trigo.apply_clockwise_to_trigo(crossSpectraRePol)
        crossSpectraRePol = spectrum_rotation.apply_rotation(crossSpectraRePol,-90.)  # This is for having origin at North # 1dec21 change +90 to -90 on a case descending
        crossSpectraRePol = spectrum_rotation.apply_rotation(crossSpectraRePol,heading)
        core_plot_part(crossSpectraRePol,heading,fig=fig,ax=ax,thecmap=thecmap,
                       limit_display_wv=limit_display_wv,title=title,
                       tauval=tauval)

        if outputfile:
            plt.savefig(outputfile)
            logging.info('outputfile : %s',outputfile)
        else:
            if interactive:
                plt.show()

def display_cartesian_spectra(allspecs,part='Re',limit_display_wv=100,title=None,outputfile=None,
                          fig=None,ax=None,interactive=False,tau_id=2):
    for tauval in [tau_id] :
        if part=='Im':
            thecmap='PuOr'
            coS = allspecs['cross-spectrum_%stau' % tauval].mean(
                dim='%stau' % tauval).imag  # co spectrum = imag part of cross-spectrum
        else:
            thecmap=cmap
            coS = abs(allspecs['cross-spectrum_%stau' % tauval].mean(
                dim='%stau' % tauval).real)  # co spectrum = real part of cross-spectrum


        crossSpectraRe_red = coS.where(
            np.logical_and(np.abs(coS.kx) <= 0.14,np.abs(coS.ky) <= 0.14),drop=True)
        crossSpectraRe_red = crossSpectraRe_red.rolling(kx=3,center=True).mean().rolling(ky=3,center=True).mean()
        #crossSpectraRePol = new_spec_Polar.squeeze()
        # crossSpectraRePol = spectrum_clockwise_to_trigo.apply_clockwise_to_trigo(crossSpectraRePol)
        # crossSpectraRePol = spectrum_rotation.apply_rotation(crossSpectraRePol,
        #                                                       90.)  # This is for having origin at North
        #crossSpectraRePol = spectrum_rotation.apply_rotation(crossSpectraRePol,heading)

        # partie display plot
        if fig is None:
            plt.figure(figsize=(10,8),dpi=100)
        if ax is None:

            ax = plt.subplot(1,1,1,polar=False)

            #ax.set_theta_direction(-1)  # theta increasing clockwise
           # ax.set_theta_zero_location("N")


        #shift_phi = np.radians(heading-90)


        #crossSpectraRe_redpol_phi_new = crossSpectraRe_redpol.assign_coords({'phi':(crossSpectraRe_redpol.phi.values+shift_phi)[::-1]})
        # crossSpectraRe_redpol_phi_new = crossSpectraRe_redpol_phi_new/45000.
        crossSpectraRe_red.plot(cmap=thecmap,alpha=0.8)
        plt.grid(True)

        #display_xspectra_grid.circle_plot(ax,r=[300,400,500,600])
        #ax.set_rmax(2.0*np.pi/limit_display_wv)
        if title is None:
            plt.title('tau : %s' % tauval,fontsize=18)
        else:
            plt.title(title,fontsize=18)
        if outputfile:
            plt.savefig(outputfile)
            logging.info('outputfile : %s',outputfile)
        else:
            if interactive:
                plt.show()

