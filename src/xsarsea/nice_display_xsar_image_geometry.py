import numpy as np

from detrend import sigma0_detrend
import logging
from matplotlib import pyplot as plt
def show_nice_display_wv(dsslc):
    """

    :param dsslc: xsar xarray.Dataset or xsar.Sentinel1Dataset
    :return:
    """
    s0ND = sigma0_detrend(dsslc['sigma0'], dsslc['incidence'], wind_speed_gmf=10, wind_dir_gmf=45)
    s0ND = s0ND.isel({'range':slice(1000,-1000),'azimuth':slice(1000,-1000)})
    per95 = np.nanpercentile(s0ND, 99)
    logging.info('s0ND: %s %s %s', type(s0ND), s0ND.shape,s0ND)
    s0ND = s0ND.rolling(range=5, center=True).mean().rolling(azimuth=5, center=True).mean().compute()
    logging.info('s0ND after: %s %s', type(s0ND), s0ND.shape)
    plt.figure(figsize=(8,8),dpi=99)

    logging.info('per95 = %s',per95)
    s0ND.plot(cmap='Greys_r',vmax=per95)