import cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
from shapely.ops import transform as geom_transform
import matplotlib.ticker as mticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
import cartopy
import numpy as np
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER

import os
def show_nice_display(slc,one_wv):
    lonmin = -180
    lonmax = 180
    latmin = -75
    latmax = 75

    lonmin = slc['longitude'].min()
    lonmax = slc['longitude'].max()
    latmin = slc['latitude'].min()
    latmax = slc['latitude'].max()
    var_name_lon = 'longitude'
    var_name_lat = 'latitude'
    delta = 0.1
    width, height = 15, 12
    plt.figure(figsize=(15, 12), dpi=150)
    im_ratio = width / height
    ax = plt.axes(projection=ccrs.PlateCarree())
    ax.set_extent([lonmin - delta, lonmax + delta, latmin - delta, latmax + delta])
    sli = slice(0, None, 20)

    sigdb = 10 * np.log10(slc['digital_number'].values.squeeze()[sli, sli])
    sigdb = slc['digital_number'].values.squeeze()[sli, sli]
    plt.title('test xsar SLC WV\n%s\n nb nan : %s' % (os.path.basename(one_wv), np.isnan(sigdb).sum()))
    im = plt.contourf(slc[var_name_lon].values[sli, sli], slc[var_name_lat].values[sli, sli], sigdb, 50)
    #     im = plt.scatter(slc[var_name_lon] ,ds[var_name_lat],s=34,c=y,
    #                 label='%s %s'%(wv,len(ds[var_name_lon][mask])),antialiased=True,alpha=0.9,cmap='seismic',lw=1,edgecolor='k',vmin=-2,vmax=2)
    land_50m = cfeature.NaturalEarthFeature('physical', 'land', '10m',
                                            edgecolor='face',
                                            facecolor=cfeature.COLORS['land'])

    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                      linewidth=2, color='gray', alpha=0.5, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_left = False
    gl.xlines = True

    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': 15, 'color': 'gray'}
    gl.xlabel_style = {'color': 'gray', 'weight': 'bold'}
    gl.ylabel_style = {'size': 15, 'color': 'gray'}
    gl.ylabel_style = {'color': 'gray', 'weight': 'bold'}
    ax = plt.gca()
    #     divider = make_axes_locatable(ax)
    #     cax = divider.append_axes("right", size="5%", pad=0.05)
    ax.coastlines(antialiased=True)
    cb = plt.colorbar(im, fraction=0.016 * im_ratio, pad=0.04)
    cb.set_label('$\sigma0$  [] ')
    plt.legend()
    plt.show()