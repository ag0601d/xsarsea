#!/usr/bin/env python
# coding=utf-8
"""
"""

import logging
import pdb

import numpy as np
from scipy.signal import hann
from scipy.fftpack import fftshift, fftfreq, fft2, ifft2, ifftshift, ifft
from scipy.fftpack.helper import next_fast_len

from sar_tops_osw.utils.signal import cacfar, gaussian_lowpass

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

CLIGHT = 299792458.


class SARSpectrum(object):
    """SAR spectrum class.
    """

    def __init__(self, sarim):
        """
        sarim is xarray.Dataset from xsar lib
        """
        self.sarim = sarim
        product = sarim.s1meta.product
        if product != 'SLC':
            raise Exception('SLC product expected.')
        self.mode = sarim.s1meta.swath
        self.nburst = 0
        if self.mode in ['IW', 'EW']:
            #self.nburst = sarim.s1meta.number_of_bursts
            self.nburst = sarim.s1meta._bursts.burst.size
        self.grid_extents = None
        self.grid_ibursts = None
        self.overlap_grid_extents = None
        self.overlap_nlines = None

    def _extent_burst(self, burst_indice, valid=True):
        """
        Get extent for a SAR image burst.
        Parameters
        ----------
        burst_indice: int

        Returns
        -------
        nd.array:
            extent of the burst considered
        """
        nbursts = self.sarim.s1meta._bursts['burst'].size
        if nbursts == 0:
            raise Exception('No bursts in SAR image')
        if burst_indice < 0 or burst_indice >= nbursts:
            raise Exception('Invalid burst index number')
        if valid is True:
            burst_list = self.sarim.s1meta._bursts
            extent = np.copy(burst_list['valid_location'].values[burst_indice, :])
        else:
            #extent = self._extent_max()
            extent = [self.sarim.atrack[0],self.sarim.xtrack[0],self.sarim.atrack[-1],self.sarim.atrack[-1]]
            nlines = self.sarim.s1meta._bursts.attrs['atrack_per_burst']
            extent[0:3:2] = [nlines*burst_indice, nlines*(burst_indice+1)-1]
        return extent

    def set_grid_extents(self, extent_length=(10000., 10000.)):
        """
        """
        if self.nburst == 0:
            im_shape = np.array([self.sarim.number_of_lines,
                                 self.sarim.number_of_samples])
            #im_spacing = self.sarim.ground_spacing()
            #im_spacing = self.sarim.s1meta.image['pixel_spacing']
            #im_spacing = self.sarim._ground_spacing()
            #range_ground_spacing = self.sarim['range_ground_spacing'].isel({})
            im_spacing =  self.sarim.s1meta.image['ground_pixel_spacing']
            nextents = np.round(im_shape * im_spacing / np.array(extent_length)).astype('int32')
            spec_lim = [np.round(np.linspace(0, im_shape[i], num=nextents[i] + 1)).astype('int32') \
                        for i in range(2)]
            extents = np.zeros((nextents[0], nextents[1], 4), dtype='int32')
            extents[:, :, 0] = spec_lim[0][:-1, np.newaxis]
            extents[:, :, 2] = spec_lim[0][1:, np.newaxis] - 1
            extents[:, :, 1] = spec_lim[1][np.newaxis, :-1]
            extents[:, :, 3] = spec_lim[1][np.newaxis, 1:] - 1
            self.grid_extents = extents
        else:
            #im_spacing = self.sarim.ground_spacing()
            #im_spacing = self.sarim.s1meta.image['pixel_spacing']

            # midatrack = int((extent[2] - extent[0]) / 2)
            # midxtrack = int((extent[3] - extent[1]) / 2)
            # range_ground_spacing = self.sarim['range_ground_spacing'].isel({'atrack':})
            im_spacing = self.sarim.s1meta.image['ground_pixel_spacing']
            #im_spacing = self.sarim._ground_spacing()
            logging.debug('im_spacing : %s',im_spacing)
            #self.sarim.s1meta.fill_swathtiming() #je lis les info pour les bursts dans les annotations
            #burst_list = self.sarim.get_info('burst_list')
            #burst_list = self.sarim.s1meta.swathtiming['burst_list']
            burst_list = self.sarim.s1meta._bursts
            ran_start = burst_list['valid_location'].values[:, 1].min()
            ran_stop = burst_list['valid_location'].values[:, 3].max()
            ran_size = ran_stop - ran_start + 1
            nran = np.round(ran_size * im_spacing[1] / extent_length[1]).astype('int32')
            ran_lim = np.round(np.linspace(ran_start, ran_stop + 1, num=nran + 1)).astype('int32')
            #azi_lims = []
            extents = []
            ibursts = []
            for iburst in range(self.nburst):
                #burst_extent = self.sarim._extent_burst(iburst, valid=True)
                burst_extent = self._extent_burst(iburst,valid=True)
                azi_start = burst_extent[0]
                azi_stop = burst_extent[2]
                azi_size = azi_stop - azi_start + 1
                nazi = np.round(azi_size * im_spacing[0] / extent_length[0]).astype('int32')
                azi_lim = np.round(np.linspace(azi_start, azi_stop + 1, num=nazi + 1)).astype('int32')
                _extents = np.zeros((nazi, nran, 4), dtype='int32')
                _extents[:, :, 0] = azi_lim[:-1, np.newaxis]
                _extents[:, :, 2] = azi_lim[1:, np.newaxis] - 1
                _extents[:, :, 1] = np.maximum(ran_lim[np.newaxis, :-1], burst_extent[1])
                _extents[:, :, 3] = np.minimum(ran_lim[np.newaxis, 1:] - 1, burst_extent[3])
                extents.append(_extents)
                #azi_lims.append(azi_lim)
                ibursts.extend([iburst] * nazi)
            #extents = np.zeros((len(ibursts), nran, 4), dtype='int32')
            #extents[:, :, 1] = ran_lim[np.newaxis, :-1]
            #extents[:, :, 3] = ran_lim[np.newaxis, 1:] - 1
            #extents[:, :, 0] = np.concatenate([azi_lim[:-1] for azi_lim in azi_lims])[:, np.newaxis]
            #extents[:, :, 2] = np.concatenate([azi_lim[1:] - 1 for azi_lim in azi_lims])[:, np.newaxis]
            self.grid_extents = np.concatenate(extents, axis=0)
            self.grid_ibursts = np.array(ibursts)

    def get_overlap_nlines(self, integer=True):
        """
        """
        if self.nburst == 0:
            raise Exception('For TOPS only.')
        burst_list = self.sarim.s1meta._bursts
        azi_time = burst_list['azimuthTime'].values
        burst_nlines = self.sarim.s1meta._bursts.attrs['atrack_per_burst']
        azi_dtime = self.sarim.s1meta.image['azimuth_time_interval']
        logging.debug('azi_dtime %s %s',azi_dtime,type(azi_dtime))
        pico_azi_dtime = azi_dtime*1e12
        pico_azi_dtime_int = int(pico_azi_dtime)
        logging.debug('pico_azi_dtime_int %s %s diff %s',pico_azi_dtime_int,type(pico_azi_dtime_int),pico_azi_dtime_int-pico_azi_dtime)
        azi_dtime = np.timedelta64(pico_azi_dtime_int,'ps')
        burst_azi_time0 = azi_time[1:]
        burst_azi_time1 = azi_time[:-1] + burst_nlines * azi_dtime
        overlap_nlines = (burst_azi_time1 - burst_azi_time0) / azi_dtime
        if integer == True:
            overlap_nlines = np.round(overlap_nlines).astype('int')
        return overlap_nlines

    def set_overlap_grid_extents(self, extent_length=10000.):
        """
        """
        if self.nburst == 0:
            raise Exception('For TOPS only.')
        #im_spacing = self.sarim.ground_spacing()
        #im_spacing = self.sarim.s1meta.image['pixel_spacing']
        #im_spacing = self.sarim._ground_spacing()
        im_spacing = self.sarim.s1meta.image['ground_pixel_spacing']
        burst_list = self.sarim.s1meta._bursts
        burst_nlines = self.sarim.s1meta._bursts.attrs['atrack_per_burst']
        overlap_nlines = self.get_overlap_nlines()

        ran_start = burst_list['valid_location'].values[:, 1].min()
        ran_stop = burst_list['valid_location'].values[:, 3].max()
        ran_size = ran_stop - ran_start + 1
        nran = np.round(ran_size * im_spacing[1] / extent_length).astype('int32')
        ran_lim = np.round(np.linspace(ran_start, ran_stop + 1, num=nran + 1)).astype('int32')

        bursts_lim = np.arange(1, self.nburst) * burst_nlines
        extents = np.zeros((self.nburst - 1, nran, 4, 2), dtype='int32')
        for iburst in range(self.nburst - 1):
            bursts_extent = burst_list['valid_location'].values[iburst: iburst + 2, :]
            b1_nnovalid = bursts_lim[iburst] - 1 - bursts_extent[0, 2]
            b2_nnovalid = bursts_extent[1, 0] - bursts_lim[iburst]
            # b1 azi extent
            extents[iburst, :, 0, 0] = bursts_lim[iburst] - overlap_nlines[iburst] + b2_nnovalid
            extents[iburst, :, 2, 0] = bursts_extent[0, 2]
            # b2 azi extent
            extents[iburst, :, 0, 1] = bursts_extent[1, 0]
            extents[iburst, :, 2, 1] = bursts_lim[iburst] + overlap_nlines[iburst] - b1_nnovalid - 1
            # ran extent
            extents[iburst, :, 1, :] = np.maximum(ran_lim[:-1, np.newaxis],
                                                  bursts_extent[:, 1].max())
            extents[iburst, :, 3, :] = np.minimum(ran_lim[1:, np.newaxis] - 1,
                                                  bursts_extent[:, 3].min())
        self.overlap_grid_extents = extents
        self.overlap_nlines = overlap_nlines

    def get_grid_shape(self):
        """
        """
        if self.grid_extents is None:
            return None
        else:
            return (self.grid_extents.shape[0], self.grid_extents.shape[1])

    def get_overlap_grid_shape(self):
        """
        """
        if self.overlap_grid_extents is None:
            return None
        else:
            return (self.overlap_grid_extents.shape[0],
                    self.overlap_grid_extents.shape[1])

    def get_grid_extent(self, grid_ind):
        """
        """
        if self.grid_extents is None:
            raise Exception('Call set_grid_extents first.')
        return self.grid_extents[grid_ind[0], grid_ind[1], :]

    def get_overlap_grid_extent(self, overlap_grid_ind):
        """
        """
        if self.overlap_grid_extents is None:
            raise Exception('Call set_overlap_grid_extents first.')
        return self.overlap_grid_extents[overlap_grid_ind[0], overlap_grid_ind[1], :, :]

    def extent_max(self):
        """Get extent for the whole SAR image.
        copy/pasted from cerbere
        """
        return np.array((0, 0, self.sarim.s1meta.image['shape'][0] - 1,
                         # TODO see whether it is still needed if gcp a set on integer index (instead of x.5 index)
                         self.sarim.s1meta.image['shape'][1] - 1))


    def _guess_extent(self, extent, grid_ind):
        """
        """
        if extent is not None:
            return extent
        elif grid_ind is not None:
            # in order to patch the one pixel size difference between xsar and cerbere (xsar smaller) I patch here the extent
            res = self.get_grid_extent(grid_ind)
            res[0] -= 1
            res[2] -= 1
            #res[1] += 1
            #res[3] += 1
            return res
        elif self.mode == 'WV':

            return self.extent_max()
        else:
            raise Exception('Specify extent or grid_ind (for grid_ind call set_grid_extents first).')

    def get_ccs(self, extent=None, grid_ind=None, periodo_shape=(1024, 1024),
                look_width=(0.25, 0.75), look_sep=0.27, nlooks=3,
                remove_bright=True, lowpass_width=[750., 750.]):
        """
        """
        ########################################################################
        # Get SAR inputs
        ########################################################################
        logger.info('Get SAR inputs')
        extent = self._guess_extent(extent, grid_ind)
        # Get common parameters
        logger.debug('vars in dataset : %s',self.sarim.dataset.keys())
        atrack_tmp = self.sarim.dataset['atrack']
        xtrack_tmp = self.sarim.dataset['xtrack']
        # get the incidence at the center of the part of image selected

        # lat = self.sarim.get_data('lat', extent=extent,
        #                           midrange=True, midazimuth=True)[0, 0]

        atrack_slice = slice(extent[0],extent[2])
        xtrack_slice = slice(extent[1],extent[3])
        midatrack = int((extent[2]-extent[0])/2)
        midxtrack = int((extent[3]-extent[1])/2)
        selecta = {'atrack': atrack_slice,'xtrack': xtrack_slice}
        logging.debug('selecta %s',selecta)
        logging.debug('midatrack,midxtrack %s %s',midatrack,midxtrack)
        lat = self.sarim.dataset['latitude'].isel(selecta).values[midatrack,midxtrack]
        lon = self.sarim.dataset['longitude'].isel(selecta).values[midatrack,midxtrack]
        inc = self.sarim.dataset['incidence'].isel(selecta).values[midatrack,midxtrack]
        heading = self.sarim.dataset.platform_heading
        _pass = self.sarim.dataset.orbit_pass
        # azi_time = self.sarim.get_data('azimuth_time', extent=extent,
        #                                midrange=True, midazimuth=True)[0, 0]
        # sltran_time = self.sarim.get_data('slant_range_time', extent=extent,
        #                                   midrange=True, midazimuth=True)[0, 0]
        azi_time = self.sarim.dataset['azimuth_time'].isel(selecta).values[midatrack,midxtrack]
        sltran_time = self.sarim.dataset['slant_range_time'].isel(selecta).values[midatrack,midxtrack]
        slant_range = sltran_time * CLIGHT / 2.
        #azi_spacing = self.sarim.get_info('azimuth_pixel_spacing')
        #azi_spacing = self.sarim.dataset.pixel_atrack_m
        #azi_spacing = self.sarim.s1meta.azimuthPixelSpacing
        #azi_spacing = self.sarim.dataset.attrs['pixel_atrack_m']
        slant_pixel_spacing = self.sarim.s1meta.image['slant_pixel_spacing']
        azi_spacing = slant_pixel_spacing[0]
        #grdran_spacing = self.sarim.dataset.pixel_xtrack_m / np.sin(np.deg2rad(inc))
        #grdran_spacing = self.sarim.dataset.attrs['pixel_xtrack_m'] / np.sin(np.deg2rad(inc))
        grdran_spacing = slant_pixel_spacing[1] / np.sin(np.deg2rad(inc))
        #sensor_vel = self.sarim.get_sensor_velocity(azi_time)
        sensor_vel = self.sarim.dataset['velocity'].isel({'atrack': atrack_slice}).values[midatrack]
        azi_time_int = self.sarim.s1meta.image['azimuth_time_interval']
        ground_vel = azi_spacing / azi_time_int
        kradar = 2. * np.pi * self.sarim.s1meta.image['radar_frequency'] / CLIGHT
        # Compute tau
        dopfmrate = kradar * sensor_vel * ground_vel / np.pi / slant_range
        # _dopfmratelist = self.sarim.get_info('azimuthfmrate_list')
        # _ind = np.abs(azi_time - _dopfmratelist['azimuth_time']).argmin()
        # _coeffs = [_dopfmratelist[c][_ind] for c in ['c2', 'c1', 'c0']]
        # _dopfmrate = np.polyval(_coeffs, sltran_time - _dopfmratelist['t0'][_ind])
        tau = look_sep / azi_time_int / dopfmrate
        # Get image
        #image = self.sarim.get_data('complex', extent=extent)
        image = self.sarim.dataset['digital_number'].isel({'atrack': slice(extent[0],extent[2]),
                                               'xtrack': slice(extent[1],extent[3]),'pol':0 #TODO fix polarization
                                               }).values
        if self.nburst != 0:
            logging.debug('extent : %s',extent)
            tmp_deramp = deramping_chirp(self.sarim, extent)[:-1,:-1] #TODO check why I have one index difference with cerbere
            logging.debug('tmp_deramp %s',tmp_deramp.shape)
            image *= tmp_deramp
        im_shape = np.array(image.shape)
        im_spacing = np.array([azi_spacing, grdran_spacing])

        ########################################################################
        # Find bright targets
        ########################################################################
        nbright = 0
        if remove_bright == True:
            logger.info('Find bright targets')
            # - Note on clutter size: it is set in order to have N samples in
            # clutter (taking into account guard area). Moreover, it is assumed
            # here an oversampling factor of 1.25 (ie 1 sample = 1.25 pixel).
            # - Note on nstddev:
            # pfa = 10. ** np.array([-12., -11., -10., -9., -8., -7., -6.])
            # scipy.stats.norm.ppf(1. - pfa, loc=0., scale=1.)
            # nstddev -> [ 7.034 6.706 6.361 5.997 5.612 5.199 4.753]
            targetsize = [1, 1]
            guardlength = 350. # meters
            guardsize = guardlength / im_spacing
            #clutterlength = 650. # meters
            nsamp = 5000
            areasamp = np.prod(1.25 * im_spacing)
            clutterlength = np.sqrt(4. / np.pi * nsamp * areasamp + guardlength ** 2.)
            clutterlength = np.maximum(clutterlength, guardlength + 1000.)
            #print clutterlength
            cluttersize = clutterlength / im_spacing
            amplitude = np.abs(image).astype('float64')
            bright_mask = cacfar(amplitude, targetsize, guardsize, cluttersize,
                                 nstddev=10., itermax=10, nstddev_neigh=5.)
            del amplitude
            nbright = bright_mask.sum()
            # logger.info('stop')
            # print nbright
            # import pdb ; pdb.set_trace()

        ########################################################################
        # Detrend
        ########################################################################
        logger.info('Detrend')
        intensity = (np.abs(image) ** 2.).astype('float64')
        if nbright != 0:
            intensity = np.ma.MaskedArray(intensity, mask=bright_mask)
        int_mean = intensity.mean()
        if lowpass_width is not None:
            lowpass_sigma = np.array(lowpass_width) / im_spacing
            lowpass = gaussian_lowpass(intensity, lowpass_sigma)
        else:
            lowpass = int_mean
        image /= np.sqrt(lowpass)
        if nbright != 0:
            image[bright_mask] /= np.abs(image[bright_mask])
        intensity /= lowpass
        del lowpass
        if nbright != 0:
            intensity = np.ma.getdata(intensity)
            intensity[bright_mask] = 1.
        int_nv = np.var(intensity) / np.mean(intensity) ** 2.
        del intensity

        ########################################################################
        # Set periodograms / looks / specs sizes and positions
        ########################################################################
        logger.info('Set periodograms/looks/spectra shapes and positions')
        periodo_shape = np.array(periodo_shape)
        if (periodo_shape > im_shape).any() == True:
            raise Exception('Periodogramm larger than image.')
        nperiodo = np.round((im_shape - periodo_shape) / (periodo_shape / 2.) + 1).astype('int32')
        # TEST
        #nperiodo = np.array([1, 1])
        # \TEST
        periodo_pos = [np.floor(np.linspace(0, im_shape[i] - periodo_shape[i], num=nperiodo[i]) \
                                + 0.5).astype('int32') for i in range(2)]
        look_width = np.array(look_width)
        look_shape = np.floor(periodo_shape * look_width + 0.5).astype('int32')
        look_pos = [np.floor((np.linspace(1 - nlooks, nlooks - 1, num=nlooks) * look_sep + \
                              1 - look_width[0]) * .5 * periodo_shape[0]).astype('int32'),
                    np.floor(0.5 * (periodo_shape[1] - look_shape[1]) + 0.5).astype('int32')]
        #
        #resamp_pow = np.ceil(np.log(look_width) / np.log(2) + 1.).astype('int32')
        #spec_shape = np.floor(2. ** resamp_pow * periodo_shape + 0.5).astype('int32')
        #
        spec_shape = np.array([next_fast_len(siz * 2) for siz in look_shape])

        ########################################################################
        # Compute co/cross-spectra
        ########################################################################
        logger.info('Compute co/cross-spectra')
        window = np.sqrt(hann(periodo_shape[0], sym=False)[:, np.newaxis] * \
                         hann(periodo_shape[1], sym=False)[np.newaxis, :])
        look_buf = np.zeros(spec_shape, dtype='complex64')
        detlooks_buf = np.zeros(np.hstack((spec_shape, nlooks)), dtype='complex64')
        U = np.zeros(np.hstack((spec_shape, nlooks)), dtype='float32')
        V = np.zeros(nlooks, dtype='float32')
        specs = np.zeros(np.hstack((spec_shape, nlooks, nlooks)), dtype='complex64')
        # import matplotlib.pyplot as plt
        # plt.figure(num=0)
        # plt.figure(num=1)
        # plt.figure(num=2)
        for azip in periodo_pos[0]:
            for ranp in periodo_pos[1]:

                pslice = [slice(azip, azip + periodo_shape[0]),
                          slice(ranp, ranp + periodo_shape[1])]
                sub = image[pslice] * window
                looks = extract_looks(sub, look_pos=look_pos, look_shape=look_shape,
                                      #azi_profile=None, ran_profile=None,
                                      azi_profile='auto', ran_profile='auto',
                                      keep_shape=False)

                for il, look in enumerate(looks):
                    look_buf[:look_shape[0], :look_shape[1]] = look
                    look_buf *= np.sqrt(look_buf.size) / np.sqrt(look.size) / np.sqrt(sub.size)
                    look_pow = np.abs(look_buf) ** 2.
                    # plt.figure(num=il)
                    # plt.plot(look_pow.mean(axis=0), '+-')
                    U[:, :, il] += look_pow
                    #V[il] += (look_pow ** 2.).sum() # for T with IDL FFT normalization
                    V[il] += (look_pow ** 2.).mean() # for T with Python FFT normalization
                    detlooks_buf[:, :, il] = fft2(np.abs(ifft2(look_buf)) ** 2.)

                for il1 in range(nlooks):
                    for il2 in range(il1 + 1):
                        specs[:, :, il1, il2] += detlooks_buf[:, :, il1] * \
                                                 np.conj(detlooks_buf[:, :, il2])

        nperall = nperiodo.prod()
        specs /= nperall
        U /= nperall
        V /= nperall
        # print detlooks_buf[0, 0, :]
        # print np.mean(U, axis=(0, 1)), V
        # print np.mean(np.abs(specs), axis=(0, 1))
        # print np.mean(np.abs(specs) ** 2., axis=(0, 1))
        # import pdb ; pdb.set_trace()

        ########################################################################
        # Remove speckle noise bias in co-spectra
        ########################################################################
        logger.info('Remove speckle noise bias in co-spectra')
        h2n_azi = np.zeros(spec_shape[0], dtype='float32')
        h2n_azi[[0, 1, -1]] = [1., 0.25, 0.25]
        h2n_ran = np.zeros(spec_shape[1], dtype='float32')
        h2n_ran[[0, 1, -1]] = [1., 0.25, 0.25]
        h2n = h2n_azi[:, np.newaxis] * h2n_ran[np.newaxis, :]
        for il in range(nlooks):
            T = np.real(fft2(np.abs(ifft2(U[:, :, il])) ** 2.))
            T = T - (T[0, 0] - 0.5 * V[il]) * h2n
            #import matplotlib.pyplot as plt
            #plt.plot(T[0, :] / T[0, 0], '+-')
            #plt.plot(U[:, :, il].mean(axis=0), '+-')
            #plt.plot(U[:, :, il].mean(axis=1), '+-')
            specs[:, :, il, il] = remove_bias(specs[:, :, il, il], T,
                                              look_shape, order=6)

        ########################################################################
        # Weight co/cross spectra
        ########################################################################
        logger.info('Weight co/cross spectra')
        # Remove DC value
        dcval = specs[0, 0, :, :].copy()
        #specs[0, 0, :, :] = 0.
        specs[np.array([0, 1, -1])[:, np.newaxis],
              np.array([0, 1, -1])[np.newaxis, :], :, :] = 0.
        # Weight
        codcval = dcval[np.arange(nlooks), np.arange(nlooks)]
        cospecs = specs[:, :, np.arange(nlooks), np.arange(nlooks)]
        wazi = np.abs(np.sum(cospecs, axis=(1, 2)))
        intvalue = np.sum(wazi.reshape((-1, 1, 1)) * cospecs,
                          axis=(0, 1)) / codcval
        rho = np.sqrt(np.abs(intvalue / intvalue.max()))
        ccs = np.zeros(np.hstack((spec_shape, nlooks)), dtype='complex64')
        norm = np.zeros(nlooks, dtype='float32')
        for n in range(nlooks):
            for m in range(n + 1):
                # TMP
                #if n == 2 and m == 1:
                #if n == 1 and m == 0:
                #    continue
                # \TMP
                ccs[:, :, n - m] += specs[:, :, n, m] * rho[n] * rho[m] / dcval[n, m]
                norm[n - m] += (rho[n] * rho[m]) ** 2.
        # TMP
        # for n in range(nlooks):
        #     for m in range(n + 1):
        #         ccs[:, :, n - m] += specs[:, :, n, m] / dcval[n, m]
        #         norm[n - m] += 1.
        # \TMP
        ccs /= norm.reshape((1, 1, -1)) * np.sum(h2n)
        dkazi, dkran = 2. * np.pi / im_spacing / periodo_shape
        ccs /= dkazi * dkran
        # TEST
        # hy = fftshift(hann(spec_shape[0], sym=False) ** 2.)
        # hx = fftshift(hann(spec_shape[1], sym=False) ** 2.)
        # hyx = hy[:, np.newaxis] * hx[np.newaxis, :]
        # ccs2 = ccs.copy()
        # for n in range(nlooks):
        #     ccs2[:, :, n] = fftshift(fft2(ifft2(ccs2[:, :, n]) * hyx))
        # \TEST
        for n in range(nlooks):
            ccs[:, :, n] = fftshift(ccs[:, :, n])

        kazi = fftshift(fftfreq(spec_shape[0], 1.)) * spec_shape[0] * dkazi
        kran = fftshift(fftfreq(spec_shape[1], 1.)) * spec_shape[1] * dkran

        ########################################################################
        # Transfer function
        ########################################################################
        T = np.real(fft2(np.abs(ifft2(np.mean(U, axis=-1))) ** 2.))
        T = T - (T[0, 0] - 0.5 * np.mean(V)) * h2n
        tfazi = fftshift(T[:, 0] / T[0, 0])
        tfran = fftshift(T[0, :] / T[0, 0])
        # tdiff = np.abs(fftshift(T / T[0, 0]) - tfazi[:, np.newaxis] * tfran[np.newaxis, :])
        # print np.max(tdiff), np.mean(tdiff)

        ########################################################################
        # Azimuth cutoff
        ########################################################################
        cutoff_y = fftshift(fftfreq(ccs.shape[0], dkazi / 2. / np.pi))
        itau = 0
        cutoff_prof = np.real(fftshift(ifft(ifftshift(ccs[:, :, itau].mean(axis=1)))))
        cutoff_prof /= cutoff_prof.max()
        _cutoffs = np.linspace(50, 500, num=451)
        dexp = np.gradient(np.exp(-(np.pi * cutoff_y[:, np.newaxis] / \
                                    _cutoffs[np.newaxis, :]) ** 2.), axis=0)
        ind = np.sum(np.abs(np.gradient(cutoff_prof)[:, np.newaxis] - dexp),
                     axis=0).argmin()
        azi_cutoff = _cutoffs[ind]

        ########################################################################
        # Make ccs dict
        ########################################################################
        ccs_dict = {'extent': extent, 'grid_ind': grid_ind,
                    'periodo_shape': periodo_shape, 'nperiodo': nperiodo,
                    'look_width': look_width, 'look_sep': look_sep, 'nlooks': nlooks,
                    'spec_shape': spec_shape,
                    'lat': lat, 'lon': lon, 'inc': inc,
                    'heading': heading, 'pass': _pass,
                    'azi_time': azi_time, 'sltran_time': sltran_time,
                    'slant_range': slant_range,
                    'azi_spacing': azi_spacing, 'grdran_spacing': grdran_spacing,
                    'sensor_vel': sensor_vel, 'ground_vel': ground_vel,
                    'kradar': kradar, 'dopfmrate': dopfmrate, 'tau': tau,
                    'nbright': nbright,
                    'int_mean': int_mean, 'int_nv': int_nv,
                    'kazi': kazi, 'kran': kran, 'dkazi': dkazi, 'dkran': dkran,
                    'ccs': ccs,
                    #'U': U, 'V': V, 'T': T,
                    'tfazi': tfazi, 'tfran': tfran,
                    'cutoff_y': cutoff_y, 'cutoff_prof': cutoff_prof,
                    'azi_cutoff': azi_cutoff}
        return ccs_dict

    def get_overlap_cs(self, overlap_grid_ind, periodo_shape=(64, 128),
                       remove_bright=True, lowpass_width=[750., 750.]):
        """
        """
        ########################################################################
        # Get SAR inputs
        ########################################################################
        logger.info('Get SAR inputs')
        extents = self.get_overlap_grid_extent(overlap_grid_ind)
        logging.debug('extents %s',extents)
        extents = [extents[:, 0], extents[:, 1]]
        logging.debug('extents twicked %s', extents)
        # Get common parameters
        atrack_slice = slice(extents[0][0], extents[0][2])
        xtrack_slice = slice(extents[0][1], extents[0][3])
        midatrack = int((extents[0][2] - extents[0][0]) / 2)
        midxtrack = int((extents[0][3] - extents[0][1]) / 2)
        selecta = {'atrack': atrack_slice, 'xtrack': xtrack_slice}
        logging.debug('selecta %s', selecta)
        logging.debug('midatrack,midxtrack %s %s', midatrack, midxtrack)
        lat = self.sarim.dataset['latitude'].isel(selecta).values[midatrack, midxtrack]
        lon = self.sarim.dataset['longitude'].isel(selecta).values[midatrack, midxtrack]
        inc = self.sarim.dataset['incidence'].isel(selecta).values[midatrack, midxtrack]
        heading = self.sarim.dataset.platform_heading
        _pass = self.sarim.dataset.orbit_pass
        # lat = self.sarim.get_data('lat', extent=extents[0], midrange=True,
        #                           midazimuth=True)[0, 0]
        # lon = self.sarim.get_data('lon', extent=extents[0], midrange=True,
        #                           midazimuth=True)[0, 0]
        # inc = self.sarim.get_data('incidence', extent=extents[0],
        #                           midrange=True, midazimuth=True)[0, 0]
        # heading = self.sarim.get_info('platform_heading')
        # _pass = self.sarim.get_info('pass')
        azi_time = self.sarim.dataset['azimuth_time'].isel(selecta).values[midatrack, midxtrack]
        sltran_time = self.sarim.dataset['slant_range_time'].isel(selecta).values[midatrack, midxtrack]
        # azi_time = self.sarim.get_data('azimuth_time', extent=extents[0],
        #                                midrange=True, midazimuth=True)[0, 0]
        # sltran_time = self.sarim.get_data('slant_range_time', extent=extents[0],
        #                                   midrange=True, midazimuth=True)[0, 0]
        slant_range = sltran_time * CLIGHT / 2.
        #azi_spacing = self.sarim.get_info('azimuth_pixel_spacing')
        #azi_spacing = self.sarim.dataset.attrs['pixel_atrack_m']
        #grdran_spacing = self.sarim.get_info('range_pixel_spacing') / np.sin(np.deg2rad(inc))
        #grdran_spacing = self.sarim.dataset.attrs['pixel_xtrack_m'] / np.sin(np.deg2rad(inc))

        slant_pixel_spacing = self.sarim.s1meta.image['slant_pixel_spacing']
        azi_spacing = slant_pixel_spacing[0]
        grdran_spacing = slant_pixel_spacing[1] / np.sin(np.deg2rad(inc))
        #sensor_vel = self.sarim.s1meta.get_sensor_velocity(azi_time)
        sensor_vel = self.sarim.dataset['velocity'].isel({'atrack': atrack_slice}).values[midatrack]
        #azi_time_int = self.sarim.get_info('azimuth_time_interval')
        azi_time_int = self.sarim.s1meta.image['azimuth_time_interval']
        ground_vel = azi_spacing / azi_time_int
        #kradar = 2. * np.pi * self.sarim.get_info('radar_frequency') / CLIGHT
        kradar = 2. * np.pi * self.sarim.s1meta.image['radar_frequency'] / CLIGHT
        # Compute tau
        #nlineburst = self.sarim.get_info('atrack_per_burst')
        nlineburst = self.sarim.s1meta._bursts.attrs['atrack_per_burst']
        iburst0 = int(extents[0][0] / float(nlineburst))
        #burstlist = self.sarim.get_info('burst_list')
        burstlist = self.sarim.s1meta._bursts
        bursts_azi_times = burstlist['azimuthTime'].values.astype(float)
        dt_burstmid = bursts_azi_times[iburst0 + 1] - \
                      bursts_azi_times[iburst0]
        overlap_nlines = self.overlap_nlines[iburst0]
        ind_frommid = (nlineburst - overlap_nlines) / 2.
        azitime_frommid = ind_frommid * azi_time_int
        #azisteerrate = np.deg2rad(self.sarim.get_info('azimuth_steering_rate'))
        azisteerrate = np.deg2rad(self.sarim.s1meta.image['azimuth_steering_rate'])
        dt_frommid = azitime_frommid / (1. + slant_range * azisteerrate / ground_vel)
        tau = dt_burstmid - 2. * dt_frommid
        # Get image
        # images = [np.abs(self.sarim.get_data('complex', extent=ext)) ** 2. \
        #           for ext in extents]
        images = []
        for ext in extents:
            #image = self.sarim.get_data('complex', extent=ext)
            atrack_slice = slice(ext[0], ext[2])
            xtrack_slice = slice(ext[1], ext[3])
            image = self.sarim.dataset['digital_number'].isel({'atrack':atrack_slice,'xtrack':xtrack_slice,'pol':0}).values #TODO fix possibility to get the xspectra from both pola
            image *= deramping_chirp(self.sarim, ext)[:-1,:-1] #TODO check why I have one index difference with cerbere
            images.append(image)
        im_shape = images[0].shape
        im_spacing = np.array([azi_spacing, grdran_spacing])

        ########################################################################
        # Find bright targets
        ########################################################################
        nbright = 0
        if remove_bright == True:
            logger.info('Find bright targets')
            targetsize = [1, 1]
            guardlength = 350. # meters
            guardsize = guardlength / im_spacing
            #clutterlength = 650. # meters
            nsamp = 5000
            areasamp = np.prod(1.25 * im_spacing)
            clutterlength = np.sqrt(4. / np.pi * nsamp * areasamp + guardlength ** 2.)
            clutterlength = np.maximum(clutterlength, guardlength + 1000.)
            #print clutterlength
            cluttersize = clutterlength / im_spacing
            #amplitude = np.sqrt(images[0]).astype('float64')
            amplitude = np.abs(images[0]).astype('float64')
            bright_mask = cacfar(amplitude, targetsize, guardsize, cluttersize,
                                 nstddev=10., itermax=10, nstddev_neigh=5.)
            #import pdb ; pdb.set_trace()
            del amplitude
            nbright = bright_mask.sum()

        ########################################################################
        # Detrend
        ########################################################################
        logger.info('Detrend')
        int_mean = []
        int_nv = []
        for image in images:
            #intensity = image.astype('float64')
            intensity = (np.abs(image) ** 2.).astype('float64')
            if nbright != 0:
                intensity = np.ma.MaskedArray(intensity, mask=bright_mask)
            _int_mean = intensity.mean()
            int_mean.append(_int_mean)
            if lowpass_width is not None:
                lowpass_sigma = np.array(lowpass_width) / im_spacing
                lowpass = gaussian_lowpass(intensity, lowpass_sigma)
            else:
                lowpass = _int_mean
            #image /= lowpass
            image /= np.sqrt(lowpass)
            if nbright != 0:
                #image[bright_mask] = 1.
                image[bright_mask] /= np.abs(image[bright_mask])
            intensity /= lowpass
            del lowpass
            if nbright != 0:
                intensity = np.ma.getdata(intensity)
                intensity[bright_mask] = 1.
            _int_nv = np.var(intensity) / np.mean(intensity) ** 2.
            int_nv.append(_int_nv)
            del intensity

        ########################################################################
        # Periodograms sizes and positions
        ########################################################################
        logger.info('Set periodograms shapes and positions')
        periodo_shape = np.array(periodo_shape)
        #if (periodo_shape > im_shape).any() == True:
        #    raise Exception('Periodogramm larger than image.')
        nperiodo = np.round((im_shape - periodo_shape) / (periodo_shape / 2.) + 1).astype('int32')
        nperiodo = np.maximum(nperiodo, 1)
        periodo_pos = [np.floor(np.linspace(0, im_shape[i] - periodo_shape[i], num=nperiodo[i]) \
                                + 0.5).astype('int32') for i in range(2)]
        _periodo_shape = np.minimum(periodo_shape, im_shape)
        # print periodo_shape
        # print nperiodo
        # print _periodo_shape

        ########################################################################
        # Compute cross-spectrum
        ########################################################################
        logger.info('Compute cross-spectrum')
        window = hann(_periodo_shape[0], sym=False)[:, np.newaxis] * \
                hann(_periodo_shape[1], sym=False)[np.newaxis, :]
        ocs = np.zeros(periodo_shape, dtype='complex64')
        #dcval = 0.
        U = np.zeros(periodo_shape, dtype='float32')
        V = 0.
        #_specs = []
        for azip in periodo_pos[0]:
            for ranp in periodo_pos[1]:

                pslice = [slice(azip, azip + _periodo_shape[0]),
                          slice(ranp, ranp + _periodo_shape[1])]

                # ffts = [fft2(window * image[pslice], shape=periodo_shape) \
                #         for image in images]

                # means = [image[pslice].mean() for image in images]
                # subs = [image[pslice] - mean for (image, mean) in zip(images, means)]
                # ffts = [fft2(window * sub, shape=periodo_shape) for sub in subs]
                # #dcval += means[0] * means[1]

                ints = [np.abs(image[pslice]) ** 2. for image in images]
                means = [_int.mean() for _int in ints]
                subs = [_int - mean for (_int, mean) in zip(ints, means)]
                ffts = [fft2(window * sub, shape=periodo_shape) for sub in subs]

                ocs[:, :] += ffts[0] * np.conj(ffts[1])

                specs = [fft2(image[pslice] * np.sqrt(window), shape=periodo_shape) \
                         for image in images]
                aziprofs = [np.abs(spec).mean(axis=1) for spec in specs]
                dopshifts = [estimate_dopplershift(aziprof) for aziprof in aziprofs]
                specs = [np.roll(spec, -dopsh, axis=0) for (spec, dopsh) \
                         in zip(specs, dopshifts)]
                for spec in specs:
                    specpow = np.abs(spec) ** 2.
                    U += specpow
                    V += np.mean(specpow ** 2.)
                #_specs.extend(specs)

        # h2n_azi = np.zeros(periodo_shape[0], dtype='float32')
        # h2n_azi[[0, 1, -1]] = [1., 0.25, 0.25]
        # h2n_ran = np.zeros(periodo_shape[1], dtype='float32')
        # h2n_ran[[0, 1, -1]] = [1., 0.25, 0.25]
        # h2n = h2n_azi[:, np.newaxis] * h2n_ran[np.newaxis, :]
        # dcval = ocs[0, 0].copy()
        # dk = 2. * np.pi / im_spacing / periodo_shape
        # k = [fftshift(fftfreq(periodo_shape[i], im_spacing[i]) * 2. * np.pi) \
        #      for i in range(2)]
        # ocs /= np.sum(h2n) * dcval * dk[0] * dk[1]

        norm_nper = nperiodo.prod()
        norm_wind = np.mean(window ** 2.)
        norm_fft = periodo_shape.prod() ** 2.
        norm_pad = (_periodo_shape.prod() / float(periodo_shape.prod())) ** 2.
        #print norm_pad
        dkazi, dkran = 2. * np.pi / im_spacing / periodo_shape
        ocs /= norm_nper * norm_wind * norm_fft * norm_pad * dkazi * dkran
        #ocs /= dcval * norm_wind * norm_fft * norm_pad * dk[0] * dk[1]

        ocs = fftshift(ocs)

        kazi = fftshift(fftfreq(periodo_shape[0], im_spacing[0])) * 2. * np.pi
        kran = fftshift(fftfreq(periodo_shape[1], im_spacing[1])) * 2. * np.pi

        ########################################################################
        # Transfer function
        ########################################################################
        U /= nperiodo.prod() * 2.
        V /= nperiodo.prod() * 2.
        h2 = np.abs(fft2(window, shape=periodo_shape)) ** 2.
        T = np.real(fft2(np.abs(ifft2(U)) ** 2.))
        T = T - (T[0, 0] - 0.5 * V) * h2 / h2[0, 0]
        tfazi = fftshift(T[:, 0] / T[0, 0])
        tfran = fftshift(T[0, :] / T[0, 0])
        # tdiff = np.abs(fftshift(T / T[0, 0]) - tfazi[:, np.newaxis] * tfran[np.newaxis, :])
        # print np.max(tdiff), np.mean(tdiff)

        ########################################################################
        # Azimuth cutoff
        ########################################################################
        cutoff_y = fftshift(fftfreq(ocs.shape[0], dkazi / 2. / np.pi))
        cutoff_prof = np.real(fftshift(ifft(ifftshift(ocs.mean(axis=1)))))
        cutoff_prof /= cutoff_prof.max()
        _cutoffs = np.linspace(50, 500, num=451)
        dexp = np.gradient(np.exp(-(np.pi * cutoff_y[:, np.newaxis] /
                                    _cutoffs[np.newaxis, :]) ** 2.), axis=0)
        ind = np.sum(np.abs(np.gradient(cutoff_prof)[:, np.newaxis] - dexp),
                     axis=0).argmin()
        azi_cutoff = _cutoffs[ind]

        ########################################################################
        # Make ocs dict
        ########################################################################
        ocs_dict = {'extents': extents, 'grid_ind': overlap_grid_ind,
                    'periodo_shape': periodo_shape, 'nperiodo': nperiodo,
                    'lat': lat, 'lon': lon, 'inc': inc,
                    'heading': heading, 'pass': _pass,
                    'azi_time': azi_time, 'sltran_time': sltran_time,
                    'slant_range': slant_range,
                    'azi_spacing': azi_spacing, 'grdran_spacing': grdran_spacing,
                    'sensor_vel': sensor_vel, 'ground_vel': ground_vel,
                    'kradar': kradar, 'tau': tau,
                    'dt_burstmid': dt_burstmid, 'dt_frommid': dt_frommid,
                    'nbright': nbright,
                    'int_mean': int_mean, 'int_nv': int_nv,
                    'kazi': kazi, 'kran': kran, 'dkazi': dkazi, 'dkran': dkran,
                    'ocs': ocs,
                    #'U': U, 'V': V, 'T': T, 'specs': _specs, 'w': window,
                    'tfazi': tfazi, 'tfran': tfran,
                    'cutoff_y': cutoff_y, 'cutoff_prof': cutoff_prof,
                    'azi_cutoff': azi_cutoff}
        return ocs_dict


def deramping_chirp(sarim, extent):
    """
    """
    from scipy.interpolate import interp1d
    from scipy.constants import c as clight

    shape = np.array([extent[2] - extent[0] + 1, extent[3] - extent[1] + 1])

    # Identify burst index and get burst timing
    # (azitimeburst is relative to middle burst and azitimeburstmid is not)
    #nlineburst = sarim.get_info('atrack_per_burst')
    #nlineburst = sarim.s1meta.atrack_per_burst
    nlineburst = sarim.s1meta._bursts.attrs['atrack_per_burst']
    iburst01 = np.floor(np.array(extent)[[0, 2]] / float(nlineburst)).astype('int')
    if iburst01[0] != iburst01[1]:
        raise Exception('Extent covers more than one burst.')
    iburst = iburst01[0]
    #azitimeint = sarim.s1meta.azimuth_time_interval
    azitimeint = sarim.s1meta.image['azimuth_time_interval']
    azitimeint = np.timedelta64(int(azitimeint*1e12),'ps').astype('<m8[ns]') #transform float/seconds into int/timedelta picoseconds
    azitimeburst = (np.arange(shape[0]) + np.mod(extent[0], nlineburst) - \
                    (nlineburst - 1.) / 2) * azitimeint
    #burstlist = sarim.get_info('burst_list')
    #burstlist = sarim.s1meta.swathtiming['burst_list']
    burstlist = sarim.s1meta._bursts
    azitimeburstmid = burstlist['azimuthTime'].values[iburst] + \
                      azitimeint * (nlineburst - 1.) / 2
    #sltrantime0 = sarim.get_info('slant_range_time')
    sltrantime0 = sarim.s1meta.image['slant_range_time_image']
    ransamprate = sarim.s1meta.image['range_sampling_rate']
    logging.debug('extent[1] %s extent 3 %s ',extent[1], extent[3])
    logging.debug('sltrantime0 : %s',sltrantime0)
    sltrantime = sltrantime0 + np.arange(extent[1], extent[3] + 1) / ransamprate

    # Get Doppler FM rate (=ka, range dependent)
    #azifmratelist = sarim.get_info('azimuthfmrate_list')
    #azifmratelist = sarim.s1meta.azimuthfmrate
    azifmratelist = sarim.s1meta.azimuth_fmrate
    logging.debug('azimuth_time %s %s',azifmratelist['azimuth_time'],azifmratelist['azimuth_time'].dtype)
    logging.debug('azitimeburstmid : %s %s',azitimeburstmid,type(azitimeburstmid))
    iazi = np.abs(azifmratelist['azimuth_time'].values - azitimeburstmid).argmin()
    # if iazi != iburst:
    #     print 'Warning : closest azimuth FM rate polynom does not match burst index.'
    polyt0 = azifmratelist['t0'].values[iazi]
    #polycoeff = [azifmratelist[coeffname][iazi] for coeffname in ['c2', 'c1', 'c0']]
    polycoeff = azifmratelist['polynomial'].values[iazi]
    logging.debug('polycoeff : %s %s',polycoeff,dir(polycoeff))
    logging.debug('polyval coef: %s',polycoeff.coef)
    dopfmrate0 = np.polyval(polycoeff.coef[::-1], sltrantime0 - polyt0)
    dopfmrate = np.polyval(polycoeff.coef[::-1], sltrantime - polyt0)

    # Get Doppler rate due to antenna steering (=ks, constant)
    #orbstatevect = sarim.get_info('orbit_state_vectors')


    #orbstatevect = sarim.s1meta.orbit_state_vectors['orbit_state_vectors']
    #orbstatevect = sarim.s1meta.orbit
    #velos = np.array([list(uu) for uu in orbstatevect['velocity'].values])
    #velplats = np.sqrt(np.sum(velos ** 2., axis=1))
    #func = interp1d(orbstatevect.index.values.astype(float), velplats, kind='linear')
    #logging.debug('interp times orbit %s at mid burst : %s',orbstatevect.index.values.astype(float),azitimeburstmid.astype(float))
    #velplat = func(azitimeburstmid.astype(float))
    midatrack = int((extent[2]-extent[0])/2)
    velplat = sarim.dataset['velocity'].isel({'atrack':slice(extent[0],extent[2]),
                                              }).values[midatrack]
    azisteerrate = np.deg2rad(sarim.s1meta.image['azimuth_steering_rate'])
    radarfreq = sarim.s1meta.image['radar_frequency']
    dopsteerrate = 2. * velplat * radarfreq * azisteerrate / clight

    # Compute Doppler centroid rate (=kt, range dependent)
    dopctrrate = dopfmrate * dopsteerrate / (dopfmrate - dopsteerrate)

    # Get azimuth reference time (=nuref, range dependent)
    #dopctrest = sarim.get_info('doppler_centroid_estimates')
    #dopctrest = sarim.s1meta.dopplercentroid
    dopctrest = sarim.s1meta._doppler_estimate
    #iazi = np.abs(dopctrest['azimuth_time'].values[:, 0] - azitimeburstmid).argmin() #original
    iazi = np.abs(dopctrest['azimuth_time'].values[0] - azitimeburstmid).argmin()
    # if iazi != iburst:
    #     print 'Warning : closest Doppler centroid polynom does not match burst index.'
    polyt0 = dopctrest['t0'].values[iazi]
    #polycoeff = dopctrest['data_polynom'].values[iazi, ::-1]
    polycoeff = dopctrest['data_polynom'].values[iazi].coef[::-1]
    logging.debug('polycoeff : %s',polycoeff)
    logging.debug('polyt0 : %s %s',polyt0,type(polyt0))
    logging.debug('sltrantime0 : %s %s',sltrantime0,type(sltrantime0))
    dopctr0 = np.polyval(polycoeff, sltrantime0 - polyt0)
    dopctr = np.polyval(polycoeff, sltrantime - polyt0)
    azitimeref = -dopctr / dopfmrate + dopctr0 / dopfmrate0
    #azitimeref_squared = azitimeref**2
    #azitimeref_squared = (azitimeref_squared*1e9).astype(int).astype('<m8[ns]')

    # Deramp
    chirp = np.exp(-1j * np.pi * dopctrrate[np.newaxis, :] *
                   (azitimeburst[:, np.newaxis].astype(float) -
                    azitimeref[np.newaxis, :]) ** 2.)

    return chirp


def extract_looks(image, look_pos=None, look_shape=None,
                  look_width=(0.25, 0.78), look_sep=0.27, nlooks=3,
                  azi_profile='auto', ran_profile='auto', keep_shape=False):
    """
    """
    if look_pos is None or look_shape is None:
        im_shape = np.array(image.shape)
        look_width = np.array(look_width)
        look_shape = np.floor(im_shape * look_width + 0.5).astype('int32')
        look_pos = [np.floor((np.linspace(1 - nlooks, nlooks - 1, num=nlooks) * look_sep + \
                              1 - look_width[0]) * .5 * im_shape[0]).astype('int32'),
                    np.floor(0.5 * (im_shape[1] - look_shape[1]) + 0.5).astype('int32')]
    spec = fftshift(fft2(image))
    dopshift = estimate_dopplershift(np.abs(spec).mean(axis=1))
    spec = np.roll(spec, -dopshift, axis=0)
    # if azi_profile is not None:
    #     if azi_profile == 'auto':
    #         # looks_slice = [slice(look_pos[0][0], look_pos[0][-1] + look_shape[0]),
    #         #                slice(look_pos[1], look_pos[1] + look_shape[1])]
    #         azi_profile = (np.abs(spec) ** 2.).mean(axis=1)
    #         azi_profile = np.sqrt(azi_profile / azi_profile.mean())
    #     spec = spec / azi_profile[:, np.newaxis]
    # if ran_profile is not None:
    #     if ran_profile == 'auto':
    #         ran_profile = (np.abs(spec) ** 2.).mean(axis=0)
    #         ran_profile = np.sqrt(ran_profile / ran_profile.mean())
    #     spec = spec / ran_profile[np.newaxis, :]
    if azi_profile is not None and azi_profile == 'auto':
        azi_profile = np.abs(spec).mean(axis=1)
        azi_profile /= azi_profile.mean()
    if ran_profile is not None and ran_profile == 'auto':
        ran_profile = np.abs(spec).mean(axis=0)
        ran_profile /= ran_profile.mean()
    if azi_profile is not None:
        spec /= azi_profile[:, np.newaxis]
    if ran_profile is not None:
        spec /= ran_profile[np.newaxis, :]
    looks = []
    for azil in look_pos[0]:
        lslice = [slice(azil, azil + look_shape[0]),
                  slice(look_pos[1], look_pos[1] + look_shape[1])]
        if not keep_shape:
            look = spec[lslice]
        else:
            look = np.zeros_like(spec)
            look[lslice] = spec[lslice]
        looks.append(look)
    return looks


def estimate_dopplershift(profile):
    """Estimate Doppler shift from azimuth profile.
    """
    size = profile.size
    exp = np.exp(0 + 1j * 2. * np.pi * np.arange(size) / size)
    #ang = np.arctan(np.sum(profile*exp))
    pesum = np.sum(profile * exp)
    ang = np.arctan2(pesum.imag, pesum.real)
    if ang < 0:
        ang += 2 * np.pi
    return np.round(ang / 2. / np.pi * size - size / 2).astype('int32')


def remove_bias(spec, T, look_shape, order=6):
    """
    """
    eps = 0.00001
    spec_shape = np.array(spec.shape)
    #half_slice = [slice(0, siz / 2) for siz in spec_shape]
    half_slice = [slice(0, int((siz + 1) / 2)) for siz in spec_shape]
    # dout = (spec_shape - 2 * look_shape + 1) / 2
    # out_slice = [slice(siz / 2 - d, siz / 2 + d + 1) \
    #              for (siz, d) in zip(spec_shape, dout)]
    out_slice = [slice(siz, -(siz - 1)) for siz in look_shape]

    G = 0.5 * np.real(spec + np.roll(spec[:, ::-1], 1, axis=1))
    G[out_slice[0], :] = 0.
    G[:, out_slice[1]] = 0.

    W = np.zeros(spec_shape, dtype='float32')
    W[half_slice] = 1.
    FTW = fft2(T * W) # Python FFT
    #FTW = ifft2(T * W) # IDL FFT
    W[0, half_slice[1]] = 0.5
    W[half_slice[0], 0] = 0.5
    W[0, 0] = 0.25
    T2 = 4. * np.real(ifft2(FTW * np.conj(fft2(W)))) # Python FFT
    #T2 = 4. * np.real(fft2(FTW * np.conj(ifft2(W)))) # IDL FFT
    T2[out_slice[0], :] = 0.
    T2[:, out_slice[1]] = 0.
    WdT2 = W / np.maximum(T2, eps)
    WdT2[out_slice[0], :] = 0.
    WdT2[:, out_slice[1]] = 0.

    B = 0.
    for n in range(order):
        G = -4. * np.real(ifft2(FTW * np.conj(fft2(G * WdT2)))) # Python FFT
        #G = -4. * np.real(fft2(FTW * np.conj(ifft2(G * WdT2)))) # IDL FFT
        if n < order - 1:
            B = B - G
        else:
            B = B - G * 0.5
    spec_hshape = (int(spec_shape[0]/2),int(spec_shape[1]/2))
    tosli = [slice(0, spec_hshape[0] + 1), slice(-spec_hshape[1], None)]
    frsli = [slice(0, spec_hshape[0] + 1), slice(spec_hshape[1], 0, -1)]
    B[tosli] = B[frsli]
    tosli = [slice(-spec_hshape[0], None), slice(None)]
    frsli = [slice(spec_hshape[0], 0, -1), slice(None)]
    B[tosli] = B[frsli]
    # import matplotlib.pyplot as plt
    #import pdb ; pdb.set_trace()

    return spec - B
