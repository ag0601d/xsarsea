"""
Author Antoine Grouaze
creation: 4 April 2022
"""
import logging
import numpy as np
import xarray
def save_to_netcdf(output_bname,ccsds,ocsds):
    """

    save cross spectrum and associated metrics to netCDF files

    :param output_bname:
    :param ccsds:
    :param ocsds:
    :return:
    """
    ncfile_path = '{}-l1b-xsar.nc'.format(output_bname)
    stacking_intra = {}
    for kk in ccsds[0][0].keys():
        tmp = []
        for azind in range(len(ccsds)):  # ex 18
            tmp.append([])
            for raind in range(len(ccsds[0])):  # ex 5
                tmp[azind].append(ccsds[azind][raind][kk])
        tmp = np.stack(tmp)
        stacking_intra[kk] = tmp
        logging.info('stacking %s : %s', kk, stacking_intra[kk].shape)

    stacking_inter = {}
    for kk in ocsds[0][0].keys():
        tmp = []
        for azind in range(len(ocsds)):  # ex 18
            tmp.append([])
            for raind in range(len(ocsds[0])):  # ex 5
                tmp[azind].append(ocsds[azind][raind][kk])
        tmp = np.stack(tmp)
        stacking_inter[kk] = tmp
        logging.info('stacking inter %s : %s', kk, stacking_inter[kk].shape)

    gridaz_intra = np.arange(len(ccsds))
    gridra_intra = np.arange(len(ccsds[0]))
    gridaz_inter = np.arange(len(ocsds))
    gridra_inter = np.arange(len(ocsds[0]))
    kazi_intra = ccsds[0][0]['kazi']
    kazi_inter = ocsds[0][0]['kazi']
    kran_intra = ccsds[0][0]['kran']
    kran_inter = ocsds[0][0]['kran']
    image_coords = np.arange(2)  # az,range
    tau_coords = np.arange(3)
    ds = xarray.Dataset()
    ################################################## INTRA ##############################################
    ds['extent_intraburst'] = xarray.DataArray(stacking_intra['extent'],
                                               coords={'gridaz_intra': gridaz_intra, 'gridra_intra': gridra_intra,
                                                       'bounds': np.arange(4)}, dims=['gridaz_intra', 'gridra_intra', 'bounds'])
    ds['gridindice_intraburst'] = xarray.DataArray(stacking_intra['grid_ind'],
                                                   coords={'gridaz_intra': gridaz_intra, 'gridra_intra': gridra_intra,
                                                           'image_coords': image_coords},
                                                   dims=['gridaz_intra', 'gridra_intra', 'image_coords'])
    logging.info('periodo shape %s',stacking_intra['periodo_shape'].shape)
    ds['periodoshape_intraburst'] = xarray.DataArray(stacking_intra['periodo_shape'],
                                                     coords={'gridaz_intra': gridaz_intra, 'gridra_intra': gridra_intra,
                                                             'image_coords': image_coords},
                                                     dims=['gridaz_intra', 'gridra_intra', 'image_coords'])
    ds['nperiodo_intraburst'] = xarray.DataArray(stacking_intra['nperiodo'],
                                                 coords={'gridaz_intra': gridaz_intra, 'gridra_intra': gridra_intra,
                                                         'image_coords': image_coords},
                                                 dims=['gridaz_intra', 'gridra_intra', 'image_coords'])
    # ds['nperiodo_intraburst'] = xarray.DataArray(stacking_intra['nperiodo'],
    #                                              coords={'gridaz_intra': gridaz_intra, 'gridra_intra': gridra_intra},
    #                                              dims=['gridaz_intra', 'gridra_intra'])
    ds['cross_spectra_intraburst_real'] = xarray.DataArray(stacking_intra['ccs'].real,
                                                           coords={'gridaz_intra': gridaz_intra, 'gridra_intra': gridra_intra,
                                                                   'kazi_intra': kazi_intra,
                                                                   'kran_intra': kran_intra,
                                                                   'tau_coords': tau_coords},
                                                           dims=['gridaz_intra', 'gridra_intra', 'kazi_intra', 'kran_intra',
                                                                 'tau_coords'])  # -> #ccsds
    ds['cross_spectra_intraburst_imag'] = xarray.DataArray(stacking_intra['ccs'].imag,
                                                           coords={'gridaz_intra': gridaz_intra, 'gridra_intra': gridra_intra,
                                                                   'kazi_intra': kazi_intra,
                                                                   'kran_intra': kran_intra,
                                                                   'tau_coords': tau_coords},
                                                           dims=['gridaz_intra', 'gridra_intra', 'kazi_intra', 'kran_intra',
                                                                 'tau_coords'])  # -> #ccsds
    for vv in ['look_sep','nlooks','lat','lon','inc','heading','pass','azi_time','sltran_time','slant_range','azi_spacing',
               'grdran_spacing','sensor_vel','ground_vel','kradar','dopfmrate','tau','nbright','int_mean',
               'int_nv','dkazi','dkran','azi_cutoff']:
        ds[vv+'_intra'] = xarray.DataArray(stacking_intra[vv],coords={'gridaz_intra': gridaz_intra, 'gridra_intra': gridra_intra},
                                                           dims=['gridaz_intra', 'gridra_intra'])
    logging.info('cutoff_y_intra : %s',stacking_intra['cutoff_y'].shape)
    ds['cutoff_y_intra'] = xarray.DataArray(stacking_intra['cutoff_y'],coords={'gridaz_intra': gridaz_intra, 'gridra_intra': gridra_intra,
                                                                               'kazi_intra':kazi_intra},
                                                           dims=['gridaz_intra', 'gridra_intra','kazi_intra'])
    ds['cutoff_prof_intra'] = xarray.DataArray(stacking_intra['cutoff_prof'],coords={'gridaz_intra': gridaz_intra, 'gridra_intra': gridra_intra,
                                                                               'kazi_intra':kazi_intra},
                                                           dims=['gridaz_intra', 'gridra_intra','kazi_intra'])
    # ds['cutoff_prof_intra'] = xarray.DataArray(stacking_intra['cutoff_prof'],coords={'gridaz_intra': gridaz_intra, 'gridra_intra': gridra_intra,
    #                                                                            'kazi_intra':kazi_intra},
    #                                                        dims=['gridaz_intra', 'gridra_intra','kazi_intra'])
    logging.info('tazi intra %s',stacking_intra['tfazi'].shape)
    ds['tfazi_intra'] = xarray.DataArray(stacking_intra['tfazi'],coords={'gridaz_intra': gridaz_intra, 'gridra_intra': gridra_intra,
                                                                               'kazi_intra':kazi_intra},
                                                           dims=['gridaz_intra', 'gridra_intra','kazi_intra'])
    ds['tfran_intra'] = xarray.DataArray(stacking_intra['tfran'],coords={'gridaz_intra': gridaz_intra, 'gridra_intra': gridra_intra,
                                                                               'kran_intra':kran_intra},
                                                           dims=['gridaz_intra', 'gridra_intra','kran_intra'])

    ##################################################"" INTER ##############################################
    ds['cross_spectra_interburst_real'] = xarray.DataArray(stacking_inter['ocs'].real,
                                                           coords={'gridaz_inter': gridaz_inter, 'gridra_inter': gridra_inter,
                                                                   'kazi_inter': kazi_inter,
                                                                   'kran_inter': kran_inter,
                                                                   },
                                                           dims=['gridaz_inter', 'gridra_inter', 'kazi_inter', 'kran_inter'])  # -> ocsds
    ds['cross_spectra_interburst_imag'] = xarray.DataArray(stacking_inter['ocs'].imag,
                                                           coords={'gridaz_inter': gridaz_inter, 'gridra_inter': gridra_inter,
                                                                   'kazi_inter': kazi_inter,
                                                                   'kran_inter': kran_inter,
                                                                   },
                                                           dims=['gridaz_inter', 'gridra_inter', 'kazi_inter', 'kran_inter'])  # -> ocsds
    for vv in ['lat','lon','inc','heading','pass','azi_time','sltran_time','slant_range','azi_spacing',
               'grdran_spacing','sensor_vel','ground_vel','kradar','tau','dt_burstmid','dt_frommid','nbright',
               'dkazi','dkran','azi_cutoff']:
        logging.info('DA -> %s',vv)
        ds[vv+'_inter'] = xarray.DataArray(stacking_inter[vv],coords={'gridaz_inter': gridaz_inter, 'gridra_inter': gridra_inter},
                                                           dims=['gridaz_inter', 'gridra_inter'])
    for vv in ['int_mean','int_nv']:
        ds[vv + '_inter'] = xarray.DataArray(stacking_inter[vv],
                                             coords={'gridaz_inter': gridaz_inter, 'gridra_inter': gridra_inter,
                                                     'nburst':np.arange(2)},
                                             dims=['gridaz_inter', 'gridra_inter','nburst'])

    ds['cutoff_y_inter'] = xarray.DataArray(stacking_inter['cutoff_y'],coords={'gridaz_inter': gridaz_inter, 'gridra_inter': gridra_inter,
                                                                               'kazi_inter':kazi_inter},
                                                           dims=['gridaz_inter', 'gridra_inter','kazi_inter'])
    ds['cutoff_prof_inter'] = xarray.DataArray(stacking_inter['cutoff_prof'],coords={'gridaz_inter': gridaz_inter, 'gridra_inter': gridra_inter,
                                                                               'kazi_inter':kazi_inter},
                                                           dims=['gridaz_inter', 'gridra_inter','kazi_inter'])
    ds['tfazi_inter'] = xarray.DataArray(stacking_inter['tfazi'],coords={'gridaz_inter': gridaz_inter, 'gridra_inter': gridra_inter,
                                                                               'kazi_inter':kazi_inter},
                                                           dims=['gridaz_inter', 'gridra_inter','kazi_inter'])
    ds['tfran_inter'] = xarray.DataArray(stacking_inter['tfran'],coords={'gridaz_inter': gridaz_inter, 'gridra_inter': gridra_inter,
                                                                               'kran_inter':kran_inter},
                                                           dims=['gridaz_inter', 'gridra_inter','kran_inter'])

    # ds['wave_height_spectra_v1'] = xarray.DataArray() # osds_v1
    # ds['wave_height_spectra_v2'] = xarray.DataArray(osds)  # osds_v1

    ds['gridindice_interburst'] = xarray.DataArray(stacking_inter['grid_ind'],
                                                   coords={'gridaz_inter': gridaz_inter, 'gridra_inter': gridra_inter,
                                                           'image_coords': image_coords},
                                                   dims=['gridaz_inter', 'gridra_inter', 'image_coords'])
    ds['extent_interburst'] = xarray.DataArray(stacking_inter['extents'],
                                               coords={'gridaz_inter': gridaz_inter, 'gridra_inter': gridra_inter,'nburst':np.arange(2),
                                                       'bounds': np.arange(4)}, dims=['gridaz_inter', 'gridra_inter','nburst', 'bounds'])
    ds['periodoshape_interburst'] = xarray.DataArray(stacking_inter['periodo_shape'],
                                                     coords={'gridaz_inter': gridaz_inter, 'gridra_inter': gridra_inter,
                                                             'image_coords': image_coords},
                                                     dims=['gridaz_inter', 'gridra_inter', 'image_coords'])
    ds['nperiodo_interburst'] = xarray.DataArray(stacking_inter['nperiodo'],
                                                     coords={'gridaz_inter': gridaz_inter, 'gridra_inter': gridra_inter,
                                                             'image_coords': image_coords},
                                                     dims=['gridaz_inter', 'gridra_inter', 'image_coords'])
    # check types
    for uu in ds:
        print('tt',uu,type(ds[uu]),ds[uu].dtype)

    ds.to_netcdf(ncfile_path)
    logging.info('output netcdf saved succesfuly %s', ncfile_path)