#!/usr/bin/env python
# coding=utf-8
"""
"""
import sys

import xarray

sys.path.append('/home1/datahome/agrouaze/git/sar_tops_osw')

import glob
import logging
import resource
import os
#import cPickle as pickle
import pickle
import time
import copy
import pdb
import xsar
import matplotlib.pyplot as plt
import numpy as np
#dans un premier temps je ne touche pas a ces dependendances
# je veux simplement que la lecture soit independante de cerbere et uniquement dependante de xsar
from sar_tops_osw.utils.factory import sarimage
#from sar_tops_osw.data.sarspectrum import SARSpectrum
from sarspectrum_tuned import SARSpectrum
from sar_tops_osw.render.palette import getColorMap
from sar_tops_osw.utils.signal import gaussian_lowpass
from sar_tops_osw.utils.osw_inversion import osw_inversion
from sar_tops_osw.utils.save_to_netcdf import save_to_netcdf
from shapely.geometry import Polygon
import cartopy.crs as ccrs
import pandas as pd
import geopandas as gpd
import geoviews as gv
import holoviews as hv
hv.extension('bokeh')
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

OUTPUTDIR = '/home1/scratch/agrouaze/test_tops_processing/'
def tops_osw_run(safe,output_dir):
    """
    """
    #safes = [
        #'/mnt/data/sar/seom_s1_dataset/bretagne_case/S1A_IW_SLC__1SDV_20180117T181305_20180117T181328_020198_022758_8E35.SAFE'#,
        #'/mnt/data/sar/seom_s1_dataset/agulhas_case/100_S1A_IW_OCN_0119A7_20160602T170104/S1A_IW_SL2__1_DV_20160602T170032_20160602T170105_011534_0119A7_1372.SAFE'#,
        #'/mnt/data/sar/seom_s1_dataset/tc_case/S1A_IW_SLC__1SDV_20170907T103019_20170907T103047_018268_01EB76_5F55.SAFE'#,
       #IRMA# '/home/datawork-cersat-public/cache/project/mpc-sentinel1/data/esa/sentinel-1a/L1/IW/S1A_IW_SLC__1S/2017/250/S1A_IW_SLC__1SDV_20170907T103019_20170907T103047_018268_01EB76_5F55.SAFE'
        #medicane 2021
       # '/home/datawork-cersat-public/cache/project/mpc-sentinel1/data/esa/sentinel-1a/L1/IW/S1A_IW_SLC__1S/2021/300/S1A_IW_SLC__1SDV_20211027T044843_20211027T044910_040300_04C67A_6EB4.SAFE'
        #'/mnt/data/sar/seom_s1_dataset/tc_case/S1A_IW_SLC__1SDV_20170907T102951_20170907T103021_018268_01EB76_AE7B.SAFE',
        #'/mnt/data/sar/seom_s1_dataset/tc_case/S1A_IW_SLC__1SDV_20170921T224510_20170921T224537_018480_01F204_F773.SAFE',
        #'/mnt/data/sar/seom_s1_dataset/tc_case/S1A_IW_SLC__1SDV_20170921T224535_20170921T224603_018480_01F204_15C6.SAFE',
        #'/mnt/data/sar/seom_s1_dataset/tc_case/S1B_IW_SLC__1SDV_20170923T104437_20170923T104504_007518_00D45F_7474.SAFE',
        #'/mnt/data/sar/seom_s1_dataset/tc_case/S1B_IW_SLC__1SDV_20170923T104502_20170923T104529_007518_00D45F_BD26.SAFE'
        #'/mnt/data/sar/seom_s1_dataset/long_swell_case/S1A_IW_SLC__1SDV_20180516T130657_20180516T130721_021930_025E2C_243B.SAFE'
        #'/mnt/data/sar/seom_s1_dataset/land_case/S1A_IW_SLC__1SDV_20180516T130516_20180516T130543_021930_025E2C_5DF8.SAFE',
        #'/mnt/data/sar/seom_s1_dataset/land_case/S1A_IW_SLC__1SDV_20180528T095640_20180528T095707_022103_0263BD_4A85.SAFE'
    #]
    safes = [safe]
    paths = []
    for safe in safes:
        subsetMETA = xsar.Sentinel1Meta(safe).subdatasets
        #_paths = glob.glob(os.path.join(safe, 'measurement', '*[4-6].tiff'))
        _paths = []
        for subswath in subsetMETA:
            _paths.append(subswath)
        #_paths = subsetMETA
        _paths.sort()
        paths += _paths
    #output_dir = '/mnt/data/sar/seom_s1_dataset/tops_osw_processing'
    #output_dir = '/mnt/data/sar/seom_s1_dataset/tops_osw_processing_for_xspec_sign'
    #output_dir = '/mnt/data/sar/seom_s1_dataset/tops_osw_processing_test1_noprofile'
    #output_dir = '/mnt/data/sar/seom_s1_dataset/tops_osw_processing_test2_2looks'
    #output_dir = '/mnt/data/sar/seom_s1_dataset/tops_osw_processing_test3_2firstlooks'
    #output_dir = '/mnt/data/sar/seom_s1_dataset/tops_osw_processing_test4_2lastlooks'

    logging.info('output_dir : %s',output_dir)
    for path in paths:
        tops_osw_processing(path, output_dir,
                            skip=[],
                            #skip=['roughness_fig', 'ospec', 'ospec_fig', 'ospec2', 'ospec2_fig'],
                            force=[]
                            #force=['ospec2_fig']
                            #force=['roughness_fig']
                            #force=['ospec', 'ospec_fig', 'ospec2', 'ospec2_fig']
                            #force=['ospec', 'ospec2']
        )




def tops_osw_processing(input_path, output_dir, skip=[], force=[]):
    """
    """
    ###########################################################################
    # Init
    ###########################################################################
    logger.info('Init')
    logger.info('Input path: {}'.format(input_path))
    #sarim = sarimage(input_path)
    #subsetMETA = xsar.Sentinel1Meta(input_path).subdatasets
    #sarim = xsar.open_dataset(input_path)
    sarim = xsar.Sentinel1Dataset(input_path)
    sarspec = SARSpectrum(sarim)
    sarspec.set_grid_extents(extent_length=(10000., 10000.))
    sarspec.set_overlap_grid_extents(extent_length=10000.)

    #safe_name = sarim.get_info('safe_name')
    safe_name = os.path.basename(sarim.dataset.name).split(':')[0]
    _output_dir = os.path.join(output_dir, safe_name)
    if not os.path.exists(_output_dir):
        os.makedirs(_output_dir)
    #_bname = os.path.splitext(os.path.basename(input_path))[0]
    _bname = os.path.basename(input_path.split(':')[1])+'_'+input_path.split(':')[2]
    output_bname = os.path.join(_output_dir, _bname)

    ###########################################################################
    # Roughness figure image geometry
    ###########################################################################
    fig_path = '{}-roughness.png'.format(output_bname)
    do_fig = 'roughness_fig' not in skip and \
             (not os.path.exists(fig_path) or 'roughness_fig' in force)
    if do_fig:
        logger.info('Make roughness figure')
        sarim_rough = xsar.Sentinel1Dataset(input_path,resolution='%sm'%500)
        fig = make_roughness_figure(sarim_rough, sarspec, res=50., orientation='north', vmax=3)
        #fig = make_roughness_figure(sarim, sarspec, res=250., orientation='north', vmax=3, notext=True)
        plt.savefig(fig_path)
        plt.close(fig)

    ###########################################################################
    # Roughness figure geographic projection
    ###########################################################################
    fig_path = '{}-roughness_geo.html'.format(output_bname)
    do_fig = 'roughness_fig' not in skip and \
             (not os.path.exists(fig_path) or 'roughness_fig' in force)
    if do_fig:
        logger.info('Make roughness map')
        sarim_rough = xsar.Sentinel1Dataset(input_path,resolution='%sm'%500)
        fig = make_roughness_figure_map(sarim_rough, sarspec,safe=os.path.basename(input_path))
        hv.save(fig, fig_path)

    ###########################################################################
    # Compute cross-spectra
    ###########################################################################
    pkl_path = '{}-xspec.pkl'.format(output_bname)
    if not os.path.exists(pkl_path) or 'xspec' in force:

        logger.info('Compute cross-spectra')
        ccsds = []
        grid_shape = sarspec.get_grid_shape()
        #grid_shape = (4, 2)
        logger.info('Grid shape: {}'.format(grid_shape))
        for gaz in range(grid_shape[0]):
            ccsds.append([])
            for gra in range(grid_shape[1]):
                logger.info('Compute grid index: %s %s %s',gaz, gra,grid_shape)
                ccsd = sarspec.get_ccs(
                    grid_ind=(gaz, gra),
                    periodo_shape=(256, 512),
                    look_width=(0.15, 0.65),
                    look_sep=0.17,
                    nlooks=3,
                    #look_width=(0.23, 0.65),
                    #look_sep=0.26,
                    #nlooks=2,
                    remove_bright=True,
                    lowpass_width=[750., 750.]
                )
                ccsds[gaz].append(ccsd)

        ocsds = []
        ogrid_shape = sarspec.get_overlap_grid_shape()
        #ogrid_shape = (1, 2)
        logger.info('Overlap grid shape: {}'.format(ogrid_shape))
        for gaz in range(ogrid_shape[0]):
            ocsds.append([])
            for gra in range(ogrid_shape[1]):
                logger.info('Compute overlap grid index: {%s %s} %s',gaz, gra,ogrid_shape)
                ocsd = sarspec.get_overlap_cs(
                    (gaz, gra),
                    periodo_shape=(128, 512),
                    remove_bright=True,
                    lowpass_width=[750., 750.]
                )
                ocsds[gaz].append(ocsd)

        with open(pkl_path, 'wb') as f:
            #pickle.dump((ccsds, ocsds), f, 2)
            pickle.dump((ccsds,ocsds),f)

    else:

        logger.info('Restore cross-spectra')
        with open(pkl_path, 'rb') as f:
            ccsds, ocsds = pickle.load(f)

    csds = ccsds + ocsds
    azi_inds = [(d[0]['extent'][0] + d[0]['extent'][2]) / 2 for d in ccsds]
    azi_inds += [(d[0]['extents'][0][0] + d[0]['extents'][1][2]) / 2 for d in ocsds]
    csds = [x for _, x in sorted(zip(azi_inds, csds), key=lambda p: p[0])]

    ###########################################################################
    # Cross-spectra figure
    ###########################################################################
    fig_path = '{}-xspecre.png'.format(output_bname)
    do_fig = 'xspec_fig' not in skip and \
             (not os.path.exists(fig_path) or 'xspec_fig' in force)
    if do_fig:
        logger.info('Make cross-spectra figures')

        #ssig = csds[0][len(csds[0]) / 2]['dkran'] * 0.85
        #ssig = csds[0][len(csds[0]) / 2]['dkran'] * 0.75
        ssig = 0.00331862219174 * 0.75

        # Real
        fig_path = '{}-xspecre.png'.format(output_bname)
        fig = make_spec_figure(csds, ntau=1, part='real', smooth_sigma=None,
                               orientation='north', vmin=0., uniq_vmax=False,
                               north_arrow=True,
                               index_pos=(0.5, 'left', 0.99, 'top'),
                               max_pos=(0.99, 'right', 0.99, 'top'),
                               nv_pos=(0.99, 'right', 0.01, 'bottom'),
                               tau_pos=(0.01, 'left', 0.01, 'bottom'))
        plt.savefig(fig_path)
        plt.close(fig)
        fig_path = '{}-xspecre_smooth.png'.format(output_bname)
        fig = make_spec_figure(csds, ntau=1, part='real', smooth_sigma=ssig,
                               orientation='north', vmin=0., uniq_vmax=False,
                               north_arrow=True,
                               index_pos=(0.5, 'left', 0.99, 'top'),
                               max_pos=(0.99, 'right', 0.99, 'top'),
                               nv_pos=(0.99, 'right', 0.01, 'bottom'),
                               tau_pos=(0.01, 'left', 0.01, 'bottom'))
        plt.savefig(fig_path)
        plt.close(fig)
        fig_path = '{}-xspecre_smooth_uniqvmax.png'.format(output_bname)
        fig = make_spec_figure(csds, ntau=1, part='real', smooth_sigma=ssig,
                               orientation='north', vmin=0., uniq_vmax=True,
                               north_arrow=True,
                               index_pos=(0.5, 'left', 0.99, 'top'),
                               max_pos=(0.99, 'right', 0.99, 'top'),
                               nv_pos=(0.99, 'right', 0.01, 'bottom'),
                               tau_pos=(0.01, 'left', 0.01, 'bottom'))
        plt.savefig(fig_path)
        plt.close(fig)

        # Imag
        fig_path = '{}-xspecim.png'.format(output_bname)
        fig = make_spec_figure(csds, ntau=1, part='imag', smooth_sigma=None,
                               orientation='north', uniq_vmin=False, uniq_vmax=False,
                               north_arrow=True,
                               index_pos=(0.5, 'left', 0.99, 'top'),
                               max_pos=(0.99, 'right', 0.99, 'top'),
                               nv_pos=(0.99, 'right', 0.01, 'bottom'),
                               tau_pos=(0.01, 'left', 0.01, 'bottom'))
        plt.savefig(fig_path)
        plt.close(fig)
        fig_path = '{}-xspecim_smooth.png'.format(output_bname)
        fig = make_spec_figure(csds, ntau=1, part='imag', smooth_sigma=ssig,
                               orientation='north', uniq_vmin=False, uniq_vmax=False,
                               north_arrow=True,
                               index_pos=(0.5, 'left', 0.99, 'top'),
                               max_pos=(0.99, 'right', 0.99, 'top'),
                               nv_pos=(0.99, 'right', 0.01, 'bottom'),
                               tau_pos=(0.01, 'left', 0.01, 'bottom'))
        plt.savefig(fig_path)
        plt.close(fig)
        # fig_path = '{}-xspecim_smooth_uniqvmax.png'.format(output_bname)
        # fig = make_spec_figure(csds, ntau=1, part='imag', smooth_sigma=ssig,
        #                        orientation='north', uniq_vmin=True, uniq_vmax=True,
        #                        north_arrow=True,
        #                        index_pos=(0.5, 'left', 0.99, 'top'),
        #                        max_pos=(0.99, 'right', 0.99, 'top'),
        #                        nv_pos=(0.99, 'right', 0.01, 'bottom'),
        #                        tau_pos=(0.01, 'left', 0.01, 'bottom'))
        # plt.savefig(fig_path)
        # plt.close(fig)

        # Phase
        fig_path = '{}-xspecph.png'.format(output_bname)
        fig = make_spec_figure(csds, ntau=1, part='phase', smooth_sigma=None,
                               orientation='north', vmin=-np.pi, uniq_vmax=np.pi,
                               north_arrow=True,
                               index_pos=(0.5, 'left', 0.99, 'top'),
                               max_pos=(0.99, 'right', 0.99, 'top'),
                               nv_pos=(0.99, 'right', 0.01, 'bottom'),
                               tau_pos=(0.01, 'left', 0.01, 'bottom'))
        plt.savefig(fig_path)
        plt.close(fig)
        # fig_path = '{}-xspecph_smooth.png'.format(output_bname)
        # fig = make_spec_figure(csds, ntau=1, part='phase', smooth_sigma=ssig,
        #                        orientation='north', vmin=-np.pi, uniq_vmax=np.pi,
        #                        north_arrow=True,
        #                        index_pos=(0.5, 'left', 0.99, 'top'),
        #                        max_pos=(0.99, 'right', 0.99, 'top'),
        #                        nv_pos=(0.99, 'right', 0.01, 'bottom'),
        #                        tau_pos=(0.01, 'left', 0.01, 'bottom'))
        # plt.savefig(fig_path)
        # plt.close(fig)

    ###########################################################################
    # Compute ocean spectra
    ###########################################################################
    pkl_path = '{}-ospec.pkl'.format(output_bname)
    do_pkl = 'ospec' not in skip and \
             (not os.path.exists(pkl_path) or 'ospec' in force)
    if do_pkl:

        logger.info('Compute ocean spectra')
        osds = []
        grid_shape = (len(csds), len(csds[0]))
        logger.info('Grid shape: {}'.format(grid_shape))
        for gaz in range(grid_shape[0]):
            osds.append([])
            for gra in range(grid_shape[1]):
                #logger.info('Compute grid index: {}'.format((gaz, gra)))
                osd = osw_inversion(csds[gaz][gra])
                osds[gaz].append(osd)

        with open(pkl_path, 'wb') as f:
            pickle.dump(osds, f)
            #pickle.dump(osds,f,2)

    elif os.path.exists(pkl_path):

        logger.info('Restore ocean spectra')
        with open(pkl_path, 'rb') as f:
            osds = pickle.load(f)
    osds_v1 = copy.copy(osds)
    ###########################################################################
    # Ocean spectra figure
    ###########################################################################
    fig_path = '{}-ospecsym.png'.format(output_bname)
    do_fig = 'ospec_fig' not in skip and \
             (not os.path.exists(fig_path) or 'ospec_fig' in force)
    if do_fig:
        logger.info('Make ocean spectra figures')

        # Sym
        fig_path = '{}-ospecsym.png'.format(output_bname)
        fig = make_spec_figure(osds, spec_name='oss', part='real',
                               kmin=2. * np.pi / 1200.,
                               orientation='north', vmin=0., uniq_vmax=False,
                               north_arrow=True,
                               index_pos=(0.5, 'left', 0.99, 'top'),
                               max_pos=(0.99, 'right', 0.99, 'top'))
        plt.savefig(fig_path)
        plt.close(fig)

        # Asym
        fig_path = '{}-ospecasym.png'.format(output_bname)
        fig = make_spec_figure(osds, spec_name='osa', part='real',
                               kmin=2. * np.pi / 1200., cmap=plt.get_cmap('PuOr'),
                               orientation='north', uniq_vmin=False, uniq_vmax=False,
                               north_arrow=True,
                               index_pos=(0.5, 'left', 0.99, 'top'),
                               max_pos=(0.99, 'right', 0.99, 'top'))
        plt.savefig(fig_path)
        plt.close(fig)

    ###########################################################################
    # Correct ambiguity removal with spectra in overlap
    ###########################################################################
    pkl_path = '{}-ospec2.pkl'.format(output_bname)
    do_pkl = 'ospec2' not in skip and \
        (not os.path.exists(pkl_path) or 'ospec2' in force)
    if do_pkl:

        logger.info('Use overlap for ambiguity removal')
        from sar_tops_osw.utils.wave import partition_ambiguity_removal
        # Identify overlaps
        ov_azis = []
        ov_inds = []
        grid_shape = (len(osds), len(osds[0]))
        for gaz in range(grid_shape[0]):
            osd = osds[gaz][0]
            if osd['kind'] == 'cocross':
                continue
            ov_azi = (osd['extents'][0][0] + osd['extents'][1][2]) / 2
            ov_azis.append(ov_azi)
            ov_inds.append(gaz)
        ov_azis = np.array(ov_azis)
        # Loop inside bursts
        for gaz in range(grid_shape[0]):
            osd = osds[gaz][0]
            if osd['kind'] == 'overlapcross':
                continue
            # Find closest overlap
            azi = (osd['extent'][0] + osd['extent'][2]) / 2
            ov_gaz = ov_inds[np.abs(ov_azis - azi).argmin()]
            for gra in range(grid_shape[1]):
                osd = osds[gaz][gra]
                ov_osd = osds[ov_gaz][gra]
                labels, params = partition_ambiguity_removal(osd['amb_labels'],
                                                             osd['amb_params'],
                                                             osd['oss'],
                                                             ov_osd['osa'])
                osd['old_labels'] = osd['labels']
                osd['labels'] = labels
                osd['old_params'] = osd['params']
                osd['params'] = params

        with open(pkl_path, 'wb') as f:
            pickle.dump(osds, f)

    elif os.path.exists(pkl_path):

        logger.info('Restore ocean spectra v2')
        with open(pkl_path, 'rb') as f:
            osds = pickle.load(f)

    ###########################################################################
    # Ocean spectra v2 figure
    ###########################################################################
    fig_path = '{}-ospec2sym.png'.format(output_bname)
    do_fig = 'ospec2_fig' not in skip and \
             (not os.path.exists(fig_path) or 'ospec2_fig' in force)
    if do_fig:
        logger.info('Make ocean spectra v2 figures')

        # Sym
        fig_path = '{}-ospec2sym.png'.format(output_bname)
        fig = make_spec_figure(osds, spec_name='oss', part='real',
                               kmin=2. * np.pi / 1200.,
                               orientation='north', vmin=0., uniq_vmax=False,
                               north_arrow=True,
                               index_pos=(0.5, 'left', 0.99, 'top'),
                               max_pos=(0.99, 'right', 0.99, 'top'))
        plt.savefig(fig_path)
        plt.close(fig)
        fig_path = '{}-ospec2symclip.png'.format(output_bname)
        fig = make_spec_figure(osds, spec_name='oss', part='real',
                               kmin=2. * np.pi / 1200.,
                               orientation='north', vmin=0., vmax=20.,
                               north_arrow=True,
                               index_pos=(0.5, 'left', 0.99, 'top'),
                               max_pos=(0.99, 'right', 0.99, 'top'))
        plt.savefig(fig_path)
        plt.close(fig)

        # Asym
        fig_path = '{}-ospec2asym.png'.format(output_bname)
        fig = make_spec_figure(osds, spec_name='osa', part='real',
                               kmin=2. * np.pi / 1200., cmap=plt.get_cmap('PuOr'),
                               orientation='north', uniq_vmin=False, uniq_vmax=False,
                               north_arrow=True,
                               index_pos=(0.5, 'left', 0.99, 'top'),
                               max_pos=(0.99, 'right', 0.99, 'top'))
        plt.savefig(fig_path)
        plt.close(fig)
    #######################################################
    # save the different spectrum in a netCDF file (agrouaze)
    save_to_netcdf(output_bname, ccsds, ocsds)




            
# input_path = '/mnt/data/sar/seom_s1_dataset/bretagne_case/S1A_IW_SLC__1SDV_20180117T181305_20180117T181328_020198_022758_8E35.SAFE/measurement/s1a-iw2-slc-vv-20180117t181307-20180117t181327-020198-022758-005.tiff'
# output_dir = '/mnt/data/sar/seom_s1_dataset/tops_osw_processing_tmp'


def run_tops_osw_map(npart=1, kind=['cocross']):
    """
    """
    output_dir = '/mnt/data/sar/seom_s1_dataset/tops_osw_map'

    safe_dir = ['/mnt/data/sar/seom_s1_dataset/tops_osw_processing/S1A_IW_SLC__1SDV_20170907T102951_20170907T103021_018268_01EB76_AE7B.SAFE',
                '/mnt/data/sar/seom_s1_dataset/tops_osw_processing/S1A_IW_SLC__1SDV_20170907T103019_20170907T103047_018268_01EB76_5F55.SAFE']
    grd_path = ['/mnt/data/sar/seom_s1_dataset/tc_case/S1A_IW_GRDH_1SDV_20170907T102951_20170907T103020_018268_01EB76_44BE.SAFE/measurement/s1a-iw-grd-vv-20170907t102951-20170907t103020-018268-01eb76-001.tiff',
                '/mnt/data/sar/seom_s1_dataset/tc_case/S1A_IW_GRDH_1SDV_20170907T103020_20170907T103045_018268_01EB76_992F.SAFE/measurement/s1a-iw-grd-vv-20170907t103020-20170907t103045-018268-01eb76-001.tiff']
    tops_osw_processing_map(safe_dir, output_dir, grd_path, roumax=3., npart=npart, kind=kind)
    safe_dir = ['/mnt/data/sar/seom_s1_dataset/tops_osw_processing/S1A_IW_SLC__1SDV_20180117T181305_20180117T181328_020198_022758_8E35.SAFE']
    grd_path = ['/mnt/data/sar/seom_s1_dataset/bretagne_case/S1A_IW_GRDH_1SDV_20180117T181307_20180117T181328_020198_022758_9954.SAFE/measurement/s1a-iw-grd-vv-20180117t181307-20180117t181328-020198-022758-001.tiff']
    tops_osw_processing_map(safe_dir, output_dir, grd_path, roumax=3., npart=npart, kind=kind)

    safe_dir = ['/mnt/data/sar/seom_s1_dataset/tops_osw_processing/S1A_IW_SL2__1_DV_20160602T170032_20160602T170105_011534_0119A7_1372.SAFE']
    grd_path = ['/mnt/data/sar/seom_s1_dataset/agulhas_case/S1A_IW_GRDH_1SDV_20160602T170035_20160602T170100_011534_0119A7_9B81.SAFE/measurement/s1a-iw-grd-vv-20160602t170035-20160602t170100-011534-0119a7-001.tiff']
    tops_osw_processing_map(safe_dir, output_dir, grd_path, npart=npart, kind=kind)

    safe_dir = ['/mnt/data/sar/seom_s1_dataset/tops_osw_processing/S1A_IW_SLC__1SDV_20170921T224510_20170921T224537_018480_01F204_F773.SAFE',
                '/mnt/data/sar/seom_s1_dataset/tops_osw_processing/S1A_IW_SLC__1SDV_20170921T224535_20170921T224603_018480_01F204_15C6.SAFE']
    grd_path = ['/mnt/data/sar/seom_s1_dataset/tc_case/S1A_IW_GRDH_1SDV_20170921T224511_20170921T224536_018480_01F204_9FCC.SAFE/measurement/s1a-iw-grd-vv-20170921t224511-20170921t224536-018480-01f204-001.tiff',
                '/mnt/data/sar/seom_s1_dataset/tc_case/S1A_IW_GRDH_1SDV_20170921T224536_20170921T224601_018480_01F204_D0F8.SAFE/measurement/s1a-iw-grd-vv-20170921t224536-20170921t224601-018480-01f204-001.tiff']
    tops_osw_processing_map(safe_dir, output_dir, grd_path, roumax=3., npart=npart, kind=kind)

    safe_dir = ['/mnt/data/sar/seom_s1_dataset/tops_osw_processing/S1B_IW_SLC__1SDV_20170923T104437_20170923T104504_007518_00D45F_7474.SAFE',
                '/mnt/data/sar/seom_s1_dataset/tops_osw_processing/S1B_IW_SLC__1SDV_20170923T104502_20170923T104529_007518_00D45F_BD26.SAFE']
    grd_path = ['/mnt/data/sar/seom_s1_dataset/tc_case/S1B_IW_GRDH_1SDV_20170923T104439_20170923T104504_007518_00D45F_5613.SAFE/measurement/s1b-iw-grd-vv-20170923t104439-20170923t104504-007518-00d45f-001.tiff',
                '/mnt/data/sar/seom_s1_dataset/tc_case/S1B_IW_GRDH_1SDV_20170923T104504_20170923T104529_007518_00D45F_4CCD.SAFE/measurement/s1b-iw-grd-vv-20170923t104504-20170923t104529-007518-00d45f-001.tiff']
    tops_osw_processing_map(safe_dir, output_dir, grd_path, roumax=3., npart=npart, kind=kind)


def run_tops_osw_map_zoom(npart=2, kind=['cocross']):
    """
    """
    output_dir = '/mnt/data/sar/seom_s1_dataset/tops_osw_map_zoom'

    safe_dir = ['/mnt/data/sar/seom_s1_dataset/tops_osw_processing/S1B_IW_SLC__1SDV_20170923T104437_20170923T104504_007518_00D45F_7474.SAFE',
                '/mnt/data/sar/seom_s1_dataset/tops_osw_processing/S1B_IW_SLC__1SDV_20170923T104502_20170923T104529_007518_00D45F_BD26.SAFE']
    grd_path = ['/mnt/data/sar/seom_s1_dataset/tc_case/S1B_IW_GRDH_1SDV_20170923T104439_20170923T104504_007518_00D45F_5613.SAFE/measurement/s1b-iw-grd-vv-20170923t104439-20170923t104504-007518-00d45f-001.tiff',
                '/mnt/data/sar/seom_s1_dataset/tc_case/S1B_IW_GRDH_1SDV_20170923T104504_20170923T104529_007518_00D45F_4CCD.SAFE/measurement/s1b-iw-grd-vv-20170923t104504-20170923t104529-007518-00d45f-001.tiff']
    tops_osw_processing_map(safe_dir, output_dir, grd_path, roumax=3., npart=npart, kind=kind,
                            bbox=[-73, -72, 24.6, 26], rouspacing=50., ppd=2000.)


def tops_osw_processing_map(safe_dir, output_dir, grd_path=None, bbox=None, npart=1,
                            kind=['cocross'], roumin=0., roumax=2., rouspacing=500.,
                            ppd=500.):
    """
    """
    from mpl_toolkits.basemap import Basemap
    # Get results
    if not isinstance(safe_dir, (tuple, list)):
        safe_dir = [safe_dir]
    osds = []
    for _dir in safe_dir:
        pkl_paths = glob.glob(os.path.join(_dir, '*-ospec2.pkl'))
        pkl_paths.sort()
        for path in pkl_paths:
            with open(path, 'rb') as f:
                _osds = pickle.load(f)
            osds.append(_osds)
    fosds = [d for _osds in osds for ds in _osds for d in ds]
    # Get roughness
    roughness = None
    if grd_path is not None:
        if not isinstance(grd_path, (tuple, list)):
            grd_path = [grd_path]
        roughness = []
        roulon = []
        roulat = []
        for path in grd_path:
            sarim = sarimage(path)
            mspacing = (rouspacing, rouspacing)
            spacing = np.round(sarim.meters2pixels(mspacing))
            _roughness = np.sqrt(sarim.get_data('roughness', spacing=spacing))
            roughness.append(np.ma.masked_equal(_roughness, 0.))
            roulon.append(sarim.get_data('lon', spacing=spacing))
            roulat.append(sarim.get_data('lat', spacing=spacing))
    # Map bbox
    do_filter_bbox = True
    if bbox is None:
        do_filter_bbox = False
        lats = [d['lat'] for d in fosds]
        lons = [d['lon'] for d in fosds]
        mbbox = 0.25
        bbox = [min(lons) - mbbox, max(lons) + mbbox,
                min(lats) - mbbox, max(lats) + mbbox]

    ######################################################################
    # Inspect
    ######################################################################
    # Get partitions
    # plon, plat, phs, pwl, pdir = [], [], [], [], []
    # for osd in fosds:
    #     if len(osd['params']) == 0:
    #         continue
    #     npa = np.minimum(npart, len(osd['params']))
    #     _hs = [p['hs'] for p in osd['params']]
    #     asort = np.argsort(_hs)[::-1]
    #     for i in range(npa):
    #         plon.append(osd['lon'])
    #         plat.append(osd['lat'])
    #         phs.append(osd['params'][asort[i]]['hs'])
    #         pwl.append(osd['params'][asort[i]]['wl'])
    #         pdir.append(osd['params'][asort[i]]['dir'])
    # plon = np.array(plon)
    # plat = np.array(plat)
    # phs = np.array(phs)
    # pwl = np.array(pwl)
    # pdir = np.array(pdir)
    # # Plots
    # fig = plt.figure()
    # plt.scatter(pwl, phs)
    # fig = plt.figure()
    # bmap = Basemap(llcrnrlon=bbox[0], llcrnrlat=bbox[2], urcrnrlon=bbox[1],
    #                urcrnrlat=bbox[3], suppress_ticks=False, resolution='i',
    #                projection='merc')
    # bmap.fillcontinents(zorder=0)
    # #wlmin = 100.
    # #wlmax = 600.
    # ind = np.where(phs >= 0.5)
    # wlmin = pwl[ind].min()
    # wlmax = pwl[ind].max()
    # ind = np.where((pwl >= wlmin) & (pwl <= wlmax))
    # x = plon[ind]
    # y = plat[ind]
    # u = phs[ind] * np.cos(np.deg2rad(90. - pdir[ind]))
    # v = phs[ind] * np.sin(np.deg2rad(90. - pdir[ind]))
    # c = pwl[ind]
    # bmap.quiver(x, y, u, v, c, pivot='mid', latlon=True, cmap=plt.get_cmap('jet'))#, scale=400. / 0.05)#, units='xy')
    # plt.colorbar()
    # fig = plt.figure()
    # bmap = Basemap(llcrnrlon=bbox[0], llcrnrlat=bbox[2], urcrnrlon=bbox[1],
    #                urcrnrlat=bbox[3], suppress_ticks=False, resolution='i',
    #                projection='merc')
    # bmap.fillcontinents(zorder=0)
    # x = plon[ind]
    # y = plat[ind]
    # u = pwl[ind] * np.cos(np.deg2rad(90. - pdir[ind]))
    # v = pwl[ind] * np.sin(np.deg2rad(90. - pdir[ind]))
    # c = phs[ind]
    # bmap.quiver(x, y, u, v, c, pivot='mid', latlon=True, cmap=plt.get_cmap('jet'))#, scale=400. / 0.05)#, units='xy')
    # plt.colorbar()
    # plt.show()

    ######################################################################
    # 
    ######################################################################
    plon, plat, phs, pwl, pdir = [], [], [], [], []
    for osd in fosds:
        if osd['kind'] not in kind:
            continue
        if len(osd['params']) == 0:
            continue
        _hs = np.array([p['hs'] for p in osd['params']])
        _asort = np.argsort(_hs)
        for ip in range(1, npart + 1):
            if _hs.size < ip:
                continue
            plon.append(osd['lon'])
            plat.append(osd['lat'])
            phs.append(osd['params'][_asort[-ip]]['hs'])
            pwl.append(osd['params'][_asort[-ip]]['wl'])
            pdir.append(osd['params'][_asort[-ip]]['dir'])
    plon = np.array(plon)
    plat = np.array(plat)
    phs = np.array(phs)
    pwl = np.array(pwl)
    pdir = np.array(pdir)

    dpi = 100.
    figsize = [(bbox[1] - bbox[0]) * ppd / dpi, (bbox[3] - bbox[2]) * ppd / dpi]
    fig = plt.figure(figsize=figsize, dpi=dpi)
    bmap = Basemap(llcrnrlon=bbox[0], llcrnrlat=bbox[2], urcrnrlon=bbox[1],
                   urcrnrlat=bbox[3], suppress_ticks=True, resolution='i',
                   projection='merc')
    bmap.fillcontinents()#zorder=0)
    bmap.drawmeridians(np.arange(np.ceil(bbox[0]), np.floor(bbox[1]) + 1.),
                       labels=[0, 0, 0, 1])
    bmap.drawparallels(np.arange(np.ceil(bbox[2]), np.floor(bbox[3]) + 1.),
                       labels=[1, 0, 0, 0])
    if roughness is not None:
        for _rou, _lon, _lat in zip(roughness, roulon, roulat):
            bmap.pcolormesh(_lon, _lat, _rou, cmap=plt.get_cmap('Greys_r'),
                            vmin=roumin, vmax=roumax, latlon=True)
    #wlmin = 100.
    #wlmax = 600.
    indene = np.where(phs >= 0.5)
    wlmin = pwl[indene].min()
    wlmax = pwl[indene].max()
    if not do_filter_bbox:
        ind = np.where((pwl >= wlmin) & (pwl <= wlmax))
    else:
        ind = np.where((pwl >= wlmin) & (pwl <= wlmax) & \
                       (plon >= bbox[0]) & (plon <= bbox[1]) & \
                       (plat >= bbox[2]) & (plat <= bbox[3]))
    x = plon[ind]
    y = plat[ind]
    # u = phs[ind] * np.cos(np.deg2rad(90. - pdir[ind]))
    # v = phs[ind] * np.sin(np.deg2rad(90. - pdir[ind]))
    # c = pwl[ind]
    u = pwl[ind] * np.cos(np.deg2rad(90. - pdir[ind]))
    v = pwl[ind] * np.sin(np.deg2rad(90. - pdir[ind]))
    c = phs[ind]
    #bmap.quiver(x, y, u, v, c, pivot='mid', latlon=True, cmap=plt.get_cmap('jet'))#, scale=400. / 0.05)#, units='xy')
    bmap.quiver(x, y, u, v, c, pivot='tail', latlon=True, cmap=plt.get_cmap('jet'))
    plt.colorbar(shrink=0.5, label='spectral energy')
    d0 = os.path.basename(safe_dir[0]).split('_')[5]
    d1 = os.path.basename(safe_dir[-1]).split('_')[6]
    figname = 'map-{}-{}-{}part-{}.png'.format(d0, d1, npart, '_'.join(kind))
    plt.savefig(os.path.join(output_dir, figname), bbox_inches='tight')
    plt.close('all')
    #plt.show()


def test_overlap_map():
    """
    """
    #output_dir = '/mnt/data/sar/seom_s1_dataset/tops_osw_map'

    safe_dir = ['/mnt/data/sar/seom_s1_dataset/tops_osw_processing/S1A_IW_SL2__1_DV_20160602T170032_20160602T170105_011534_0119A7_1372.SAFE']
    grd_path = ['/mnt/data/sar/seom_s1_dataset/agulhas_case/S1A_IW_GRDH_1SDV_20160602T170035_20160602T170100_011534_0119A7_9B81.SAFE/measurement/s1a-iw-grd-vv-20160602t170035-20160602t170100-011534-0119a7-001.tiff']
    tops_osw_processing_map_overlap(safe_dir[0], grd_path[0])


def tops_osw_processing_map_overlap(safe_dir, grd_path=None, bbox=None, rouspacing=1000.):
    """
    """
    from mpl_toolkits.basemap import Basemap
    # Get results
    pkl_paths = glob.glob(os.path.join(safe_dir, '*-ospec2.pkl'))
    pkl_paths.sort()
    osds = []
    for path in pkl_paths:
        with open(path, 'rb') as f:
            _osds = pickle.load(f)
        osds.append(_osds)
    fosds = [d for _osds in osds for ds in _osds for d in ds]
    # TMP
    # for _osds in osds:
    #     naz, nra = len(_osds), len(_osds[0])
    #     tau = np.zeros((naz, nra), dtype='float32')
    #     for gaz in range(naz):
    #         for gra in range(nra):
    #             tau[gaz, gra] = _osds[gaz][gra]['tau']
    #     tau = tau[2::3, :]
    #     plt.figure()
    #     plt.imshow(tau, origin='lower', interpolation='nearest',
    #                cmap=plt.get_cmap('jet'))
    #     plt.colorbar()
    # plt.show()
    # /TMP
    # Get roughness
    roughness = None
    if grd_path is not None:
        sarim = sarimage(grd_path)
        #mspacing = (250., 250.)
        #mspacing = (1000., 1000.)
        mspacing = (rouspacing, rouspacing)
        spacing = np.round(sarim.meters2pixels(mspacing))
        roughness = np.sqrt(sarim.get_data('roughness', spacing=spacing))
        roulon = sarim.get_data('lon', spacing=spacing)
        roulat = sarim.get_data('lat', spacing=spacing)
        roumin, roumax = 0., 2.
        #roumin, roumax = 0., 5.
    # Map bbox
    if bbox is None:
        lats = [d['lat'] for d in fosds]
        lons = [d['lon'] for d in fosds]
        mbbox = 0.25
        bbox = [min(lons) - mbbox, max(lons) + mbbox,
                min(lats) - mbbox, max(lats) + mbbox]
    # Get residual phase speed in overlap
    plon, plat, pres, pdir = [], [], [], []
    for osd in fosds:
        if osd['kind'] == 'cocross':
            continue
        if len(osd['params']) == 0:
            continue

        labels = osd['labels']
        params = osd['params']
        ilab = np.array([p['hs'] for p in params]).argmax()
        indlab = np.where(labels == ilab + 1)

        tau = osd['tau']
        k = osd['k']
        phi = osd['phi']
        g = 9.81

        #indmax = osd['osre'][indlab].argmax()
        #phase = np.arctan2(osd['osim'], osd['osre'])
        #pspeed = phase / k[np.newaxis, :] / tau
        #pspeed_wave = np.sqrt(g / k)
        #pspeed_res = pspeed - pspeed_wave[np.newaxis, :]
        #_pspeed_res = pspeed_res[indlab[0][indmax], indlab[1][indmax]]
        #_phi = osd['phi'][indlab[0][indmax]]

        kazilab = k[indlab[1]] * np.sin(np.deg2rad(90. - phi[indlab[0]] - \
                                                   osd['heading']))
        iazilab = np.round((kazilab - osd['kazi'][0]) / osd['dkazi']).astype('int')
        kranilab = k[indlab[1]] * np.cos(np.deg2rad(90. - phi[indlab[0]] - \
                                                    osd['heading']))
        iranlab = np.round((kranilab - osd['kran'][0]) / osd['dkran']).astype('int')
        imax = np.abs(osd['ocs'][iazilab, iranlab]).argmax()
        iazimax = iazilab[imax]
        iranmax = iranlab[imax]
        _phase = np.arctan2(np.imag(osd['ocs'][iazimax, iranmax]),
                            np.real(osd['ocs'][iazimax, iranmax]))
        _k = np.sqrt(osd['kazi'][iazimax] ** 2. + osd['kran'][iranmax] ** 2.)
        _pspeed_res = _phase / _k / tau - np.sqrt(g / _k)
        _phi = params[ilab]['dir']

        plon.append(osd['lon'])
        plat.append(osd['lat'])
        pres.append(_pspeed_res)
        pdir.append(_phi)
        #if osd['grid_ind'] == (0, 2):
    plon = np.array(plon)
    plat = np.array(plat)
    pres = np.array(pres)
    pdir = np.array(pdir)

    fig = plt.figure()
    bmap = Basemap(llcrnrlon=bbox[0], llcrnrlat=bbox[2], urcrnrlon=bbox[1],
                   urcrnrlat=bbox[3], suppress_ticks=False, resolution='i',
                   projection='merc')
    bmap.fillcontinents(zorder=0)
    if roughness is not None:
        bmap.pcolormesh(roulon, roulat, roughness, cmap=plt.get_cmap('Greys_r'),
                        vmin=roumin, vmax=roumax, latlon=True)
    x = plon
    y = plat
    #u = pres * np.cos(np.deg2rad(90. - pdir))
    #v = pres * np.sin(np.deg2rad(90. - pdir))
    u = np.sign(pres) * np.cos(np.deg2rad(90. - pdir))
    v = np.sign(pres) * np.sin(np.deg2rad(90. - pdir))
    c = np.abs(pres)
    c = np.clip(c, 0., 5.)
    bmap.quiver(x, y, u, v, c, pivot='mid', latlon=True, cmap=plt.get_cmap('jet'))
    plt.colorbar()
    plt.show()

def polygons_intra_grid(sarim,sarspec):
    """

    get polygons of the intra xspec grid

    Parameters
    ----------
    sarim
    sarspec

    Returns
    -------

    """
    sha = sarspec.get_grid_shape()
    print(sha)
    indices = []
    df = pd.DataFrame()
    df['pol_intra'] = []
    all_pol = []
    labels = []
    for az in range(sha[0]):
        for ra in range(sha[1]):
            ext = sarspec.get_grid_extent((az,ra))
            az_cornes = [ext[0],ext[2],ext[2],ext[0],ext[0]]
            ra_cornes = [ext[1],ext[1],ext[3],ext[3],ext[1]]
            lons,lats = sarim.s1meta.coords2ll(az_cornes,ra_cornes)
            pol = Polygon(zip(lons,lats))
            all_pol.append(pol)
            indices.append((az,ra))
            labels.append(az + ra / 10.)
    df['pol_intra'] = (all_pol)
    df['labels'] = labels
    df.index = pd.MultiIndex.from_tuples(indices)
    geo = gpd.GeoDataFrame(df,geometry='pol_intra')
    return geo

def polygons_inter_grid(sarim,sarspec):
    ogrid_shape = sarspec.get_overlap_grid_shape()
    indices = []
    df = pd.DataFrame()
    df['pol_intra'] = []
    all_pol = []
    labels=[]
    if ogrid_shape is not None:
        for gaz in range(ogrid_shape[0]):
            for gra in range(ogrid_shape[1]):
                ext = sarspec.get_overlap_grid_extent((gaz, gra))
                for zone in range(2):
                    ra_cornes = [ext[1,zone],ext[3,zone],ext[3,zone],ext[1,zone],ext[1,zone]]
                    az_cornes = [ext[0,zone],ext[0,zone],ext[2,zone],ext[2,zone],ext[0,zone]]
                    lons, lats = sarim.s1meta.coords2ll(az_cornes, ra_cornes)
                    pol = Polygon(zip(lons, lats))
                    all_pol.append(pol)
                    #labels.append('%s,%s,%s'%(gaz,gra,zone))
                    labels.append(gaz+gra/10.)
                    indices.append((gaz, gra,zone))
        df['pol_intra'] = (all_pol)
        df['labels'] = labels
        df.index = pd.MultiIndex.from_tuples(indices)
        geo = gpd.GeoDataFrame(df, geometry='pol_intra')
    else:
        geo=None
    return geo

def make_roughness_figure_map(sarim,sarspec,safe):
    """

    Parameters
    ----------
    sarim
    sarspec
    safe

    Returns
    -------

    """
    W = 800
    H = 600
    # Plot grid
    res_intra = polygons_intra_grid(sarim, sarspec)
    poly_intra = gv.Polygons(list(res_intra.geometry), crs=ccrs.PlateCarree())
    res_inter = polygons_inter_grid(sarim,sarspec)
    poly_inter = gv.Polygons(list(res_inter.geometry), crs=ccrs.PlateCarree())
    tmptmp = [(uu['pol_intra'].centroid.x, uu['pol_intra'].centroid.y, uu['labels']) for ii, uu in res_inter.iterrows()]
    heatmap = hv.HeatMap(tmptmp)
    roughy = hv.QuadMesh((sarim.dataset['longitude'], sarim.dataset['latitude'],
                          sarim.dataset['sigma0'].isel({'pol': 0}))).opts(colorbar=True,
                                                                          width=W, height=H,
                                                                          title=os.path.basename(safe), cmap='Greys_r')
    labs = hv.Labels(heatmap).opts(hv.opts.Labels(cmap='magma', text_color='red', height=H, text_font_size='12pt', width=W))
    paths_inter =  hv.Path(poly_inter).opts(width=W, height=H,alpha=0.8,line_width=2,line_dash='dashed')
    tmptmp_intra = [(uu['pol_intra'].centroid.x, uu['pol_intra'].centroid.y, uu['labels']) for ii, uu in res_intra.iterrows()]
    heatmap_intra = hv.HeatMap(tmptmp_intra)
    labs_intra = hv.Labels(heatmap_intra).opts(hv.opts.Labels(cmap='magma', text_color='blue', height=H, text_font_size='12pt', width=W))
    fig = roughy * hv.Path(poly_intra).opts(width=W, height=H)*paths_inter*labs*labs_intra

    return fig

def make_roughness_figure(sarim, sarspec=None, res=50., orientation=None, vmax=2, notext=False):
    """
    """
    # Set orientation
    reverse = False
    if orientation == 'reverse':
        reverse = True
    elif orientation == 'north':
        #reverse = sarim.get_info('pass').lower() == 'descending'
        reserve = sarim.dataset.orbit_pass == 'Descending'
    # Get roughness
    #grd_spacing = sarim.ground_spacing()
    grd_spacing = sarim.s1meta.image['ground_pixel_spacing']
    grd_spacing = np.array(grd_spacing)
    spacing = np.maximum(np.round(res / grd_spacing), 1.) #not sure about |0] agrouaze
    #roughness = np.sqrt(sarim.get_data('roughness', spacing=spacing))
    roughness = sarim.dataset['sigma0'].isel({'pol':0})#.rolling(atrack=100,xtrack=100).mean()
    if reverse == True:
        roughness = roughness[::-1, ::-1]
    im_shape = np.array(roughness.shape)
    # Create figure and plot roughness
    fig = plt.figure(figsize=im_shape[::-1] / 100., dpi=100)
    ax = plt.axes([0., 0., 1., 1.])
    ax.imshow(roughness, origin='lower', cmap=plt.get_cmap('Greys_r'),
              vmin=0, vmax=vmax)
    ax.set_axis_off()
    # Plot grid
    if sarspec is not None:
        _spacing = spacing[[0, 1, 0, 1]]
        #_spacing = spacing #test agrouaze
        grid_shape = sarspec.get_grid_shape()
        if grid_shape is not None:
            for gaz in range(grid_shape[0]):
                for gra in range(grid_shape[1]):
                    ext = sarspec.get_grid_extent((gaz, gra))
                    ext = (ext - _spacing / 2.) / _spacing
                    if reverse == True:
                        ext = im_shape[[0, 1, 0, 1]] - 1 - ext[[2, 3, 0, 1]]
                    ax.plot(ext[[1, 3, 3, 1, 1]], ext[[0, 0, 2, 2, 0]], '-r')
                    if not notext:
                        ax.text((ext[1] + ext[3]) / 2., (ext[0] + ext[2]) / 2.,
                                '{}/{}'.format(gaz, gra), color='r',
                                horizontalalignment='center')
        ogrid_shape = sarspec.get_overlap_grid_shape()
        if ogrid_shape is not None:
            for gaz in range(ogrid_shape[0]):
                for gra in range(ogrid_shape[1]):
                    ext = sarspec.get_overlap_grid_extent((gaz, gra))
                    ext = (ext - _spacing[:, np.newaxis] / 2.) / _spacing[:, np.newaxis]
                    if reverse == True:
                        ext = im_shape[[0, 1, 0, 1], np.newaxis] - 1 - ext[[2, 3, 0, 1], :]
                        ext = ext[:, ::-1]
                    ax.plot(ext[[1, 3, 3, 1, 1], 0], ext[[0, 0, 2, 2, 0], 0], '--b')
                    ax.plot(ext[[1, 3, 3, 1, 1], 1], ext[[0, 0, 2, 2, 0], 1], '--b')
                    if not notext:
                        ax.text((ext[1, 0] + ext[3, 0]) / 2., ext[2, 1],
                                '{}/{}'.format(gaz, gra), color='b',
                                horizontalalignment='center')
    return fig


def ax_txt_corner(ax, txt, pos, **kwargs):
    """
    """
    # if pos[0] == 'b':
    #     yrat, va = 0.01, 'bottom'
    # elif pos[0] == 't':
    #     yrat, va = 0.99, 'top'
    # elif pos[0] == 'c':
    #     yrat, va = 0.50, 'center'
    # if pos[1] == 'l':
    #     xrat, ha = 0.01, 'left'
    # elif pos[1] == 'r':
    #     xrat, ha = 0.99, 'right'
    # elif pos[1] == 'c':
    #     xrat, ha = 0.50, 'left'#'center'
    xrat, ha, yrat, va = pos
    xlim = ax.get_xlim()
    xpos = xlim[0] + xrat * (xlim[1] - xlim[0])
    ylim = ax.get_ylim()
    ypos = ylim[0] + yrat * (ylim[1] - ylim[0])
    ax.text(xpos, ypos, txt, ha=ha, va=va, **kwargs)


def make_spec_figure(sds, spec_name='*cs', ntau=1, part='real', smooth_sigma=None,
                     spec_figsize=(256, 256), orientation=None, cmap=None,
                     kmax=2*np.pi/75, kmin=2*np.pi/600,
                     vmax=None, uniq_vmax=False, vmin=None, uniq_vmin=False,
                     klim=[2*np.pi/400, 2*np.pi/200, 2*np.pi/100],
                     fontsize='x-small', north_arrow=False,
                     index_pos=None, max_pos=None, nv_pos=None, tau_pos=None):
    """
    """
    if isinstance(sds, dict): # only one spectrum
        sds = [[sds]]
    nazi, nran = (len(sds), len(sds[0]))
    # Set orientation
    reverse = False
    if orientation == 'reverse':
        reverse = True
    elif orientation == 'north':
        if 'pass' in sds[0][0]:
            reverse = sds[0][0]['pass'].lower() == 'descending'
        elif 'heading' in sds[0][0]:
            reverse = np.cos(np.deg2rad(sds[0][0]['heading'])) < 0
        else:
            raise Exception("Can't guess pass.")
    # Set cmap
    if cmap is None:
        if part == 'real':
            cmap = getColorMap(rgbFile='wind.pal')
        elif part == 'imag':
            cmap = plt.get_cmap('PuOr')
        else:
            cmap = plt.get_cmap('jet')
    # Reformat spec dict inputs
    datas = []
    for iazi in range(nazi):
        datas.append([])
        for iran in range(nran):
            sd = sds[iazi][iran]
            data = {}
            if spec_name == 'ccs' or (spec_name == '*cs' and 'ccs' in sd):
                spec = sd['ccs'][:, :, ntau]
                data['coord'] = 'cart'
                data['tau'] = ntau * sd['tau']
                data['index'] = sd['grid_ind']
                data['index_color'] = 'r'
                data['nv'] = sd['int_nv']
            elif spec_name == 'ocs' or (spec_name == '*cs' and 'ocs' in sd):
                spec = sd['ocs']
                data['coord'] = 'cart'
                data['tau'] = sd['tau']
                data['index'] = sd['grid_ind']
                data['index_color'] = 'b'
                data['nv'] = np.mean(sd['int_nv'])
            elif spec_name in ['oss', 'osa']:
                spec = sd[spec_name]
                data['coord'] = 'kphi'
                data['index'] = sd['grid_ind']
                if sd['kind'] == 'cocross':
                    data['index_color'] = 'r'
                elif sd['kind'] == 'overlapcross':
                    data['index_color'] = 'b'
                if 'labels' in sd:
                    data['labels'] = sd['labels']
            else:
                raise Exception('Spectrum not found.')
            if part == 'abs':
                spec = np.abs(spec)
            elif part == 'real':
                spec = np.real(spec)
            elif part == 'imag':
                spec = np.imag(spec)
            elif part == 'phase':
                specre = np.real(spec)
                specim = np.imag(spec)
                if smooth_sigma is None:
                    spec = np.arctan2(specim, specre)
            else:
                raise Exception('Unknown part.')
            if data['coord'] == 'cart':
                kazi, kran = sd['kazi'], sd['kran']
                dkazi, dkran = sd['dkazi'], sd['dkran']
                if smooth_sigma is not None:
                    _sigma = np.array(smooth_sigma) / np.array([dkazi, dkran])
                    if part != 'phase':
                        spec = gaussian_lowpass(spec, _sigma)
                    else:
                        specre = gaussian_lowpass(specre, _sigma)
                        specim = gaussian_lowpass(specim, _sigma)
                        spec = np.arctan2(specim, specre)
                if reverse:
                    spec, kazi, kran = spec[::-1, ::-1], -kazi[::-1], -kran[::-1]
                indazi = np.where(np.abs(kazi) <= kmax)[0]
                indran = np.where(np.abs(kran) <= kmax)[0]
                spec = spec[indazi[:, np.newaxis], indran[np.newaxis, :]]
                kazi, kran = kazi[indazi], kran[indran]
                extent = [kran[0] - dkran / 2., kran[-1] + dkran / 2.,
                          kazi[0] - dkazi / 2., kazi[-1] + dkazi / 2.]
                k = np.sqrt(kazi[:, np.newaxis] ** 2. + kran[np.newaxis, :] ** 2.)
                _min = spec[k >= kmin].min()
                _max = spec[k >= kmin].max()
                data.update({'spec':spec,
                             'kazi':kazi, 'kran':kran, 'k':k,
                             'extent':extent, 'min':_min, 'max':_max})
                if 'heading' in sd:
                    north = np.deg2rad(90. + sd['heading'])
                    if reverse == True:
                        north += np.pi
                    data['north'] = north
            elif data['coord'] == 'kphi':
                k, phi = sd['k'], sd['phi']
                philim = np.concatenate([[phi[0] - (phi[1] - phi[0]) / 2.],
                                         (phi[:-1] + phi[1:]) / 2.,
                                         [phi[-1] + (phi[-1] - phi[-2]) / 2.]])
                _klim = np.concatenate([[k[0] - (k[1] - k[0]) / 2.],
                                        (k[:-1] + k[1:]) / 2.,
                                        [k[-1] + (k[-1] - k[-2]) / 2.]])
                ylim = _klim[np.newaxis, :] * np.sin(np.deg2rad(90. - philim[:, np.newaxis]))
                xlim = _klim[np.newaxis, :] * np.cos(np.deg2rad(90. - philim[:, np.newaxis]))
                _min = spec[:, k >= kmin].min()
                _max = spec[:, k >= kmin].max()
                data.update({'spec':spec,
                             'k':k, 'phi':phi, 'xlim':xlim, 'ylim':ylim,
                             'min':_min, 'max':_max})
                data['north'] = np.pi / 2.
            datas[iazi].append(data)
    # Set contrast
    if vmax is None and uniq_vmax == True:
        maxs = [d['max'] for ds in datas for d in ds]
        vmax = max(maxs)
    if vmin is None and uniq_vmin == True:
        mins = [d['min'] for ds in datas for d in ds]
        vmin = min(mins)
    # Create figure
    figsize = np.array([nran, nazi]) * np.array(spec_figsize) / 100.
    fig = plt.figure(figsize=figsize, dpi=100)
    # Plot spectra
    theta = np.linspace(0, 2. * np.pi, num=181)
    for iazi in range(nazi):
        for iran in range(nran):
            data = datas[iazi][iran]
            spec = data['spec']
            # Set axes
            rect = [iran / float(nran), iazi / float(nazi), 1. / nran, 1. / nazi]
            if reverse == True:
                rect[0] = 1. - rect[0] - 1. / nran
                rect[1] = 1. - rect[1] - 1. / nazi
            ax = plt.axes(rect)
            ax.set_ylim([-kmax, kmax])
            ax.set_xlim([-kmax, kmax])
            ax.set_axis_off()
            # Set vmin/vmax
            if vmax is not None:
                _vmax = vmax
            else:
                _vmax = data['max']
            if vmin is not None:
                _vmin = vmin
            else:
                _vmin = data['min']
            # Plot spectra
            if data['coord'] == 'cart':
                ax.imshow(spec, cmap=cmap, origin='lower', interpolation='nearest',
                          extent=data['extent'], vmin=_vmin, vmax=_vmax)
            elif data['coord'] == 'kphi':
                xlim = data['xlim']
                ylim = data['ylim']
                ax.pcolormesh(xlim, ylim, spec, cmap=cmap, vmin=_vmin, vmax=_vmax)
                if 'labels' in data:
                    # Contour labels
                    labels = data['labels']
                    ind = np.where(labels != np.roll(labels, -1, axis=0))
                    if ind[0].size != 0:
                        x = np.stack((xlim[ind[0] + 1, ind[1]], xlim[ind[0] + 1, ind[1] + 1]))
                        y = np.stack((ylim[ind[0] + 1, ind[1]], ylim[ind[0] + 1, ind[1] + 1]))
                        plt.plot(x, y, '-k')
                    ind = np.where(labels[:, :-1] != labels[:, 1:])
                    if ind[0].size != 0:
                        x = np.stack((xlim[ind[0], ind[1] + 1], xlim[ind[0] + 1, ind[1] + 1]))
                        y = np.stack((ylim[ind[0], ind[1] + 1], ylim[ind[0] + 1, ind[1] + 1]))
                        plt.plot(x, y, '-k')
                    ind = np.where(labels[:, 0] != 0)
                    if ind[0].size != 0:
                        x = np.stack((xlim[ind[0], 0], xlim[ind[0] + 1, 0]))
                        y = np.stack((ylim[ind[0], 0], ylim[ind[0] + 1, 0]))
                        plt.plot(x, y, '-k')
                    ind = np.where(labels[:, -1] != 0)
                    if ind[0].size != 0:
                        x = np.stack((xlim[ind[0], -1], xlim[ind[0] + 1, -1]))
                        y = np.stack((ylim[ind[0], -1], ylim[ind[0] + 1, -1]))
                        plt.plot(x, y, '-k')
            # Plot k
            ax.plot([-kmax, kmax], [0, 0], ':k')
            ax.plot([0, 0], [-kmax, kmax], ':k')
            if nazi > 1 or nran > 1:
                ax.plot([-kmax, kmax, kmax, -kmax, -kmax],
                        [-kmax, -kmax, kmax, kmax, -kmax], ':k')
            if klim is not None:
                for kli in klim:
                    ax.plot(kli * np.cos(theta), kli * np.sin(theta), ':k')
                    klistr = '{:d}m'.format(int(np.round(2. * np.pi / kli)))
                    ax.text(0, -kli, klistr, ha='center', va='top',
                            fontsize=fontsize)
            # Plot north arrow
            if north_arrow == True and 'north' in data:
                karr = 2. * np.pi / 600.
                ax.arrow(0, 0, karr * np.cos(data['north']), karr * np.sin(data['north']),
                         length_includes_head=True, width=0.0001, facecolor='k')
            # Print index
            if index_pos is not None:
                index = (iazi, iran)
                if 'index' in data:
                    index = data['index']
                color = 'k'
                if 'index_color' in data:
                    color = data['index_color']
                txt = '{}/{}'.format(index[0], index[1])
                ax_txt_corner(ax, txt, index_pos, fontsize=fontsize, color=color)
            # Print max
            if max_pos is not None and 'max' in data:
                txt = 'max={:0.2f}'.format(data['max'])
                ax_txt_corner(ax, txt, max_pos, fontsize=fontsize)
            # Print nv
            if nv_pos is not None and 'nv' in data:
                txt = 'nv={:0.2f}'.format(data['nv'])
                ax_txt_corner(ax, txt, nv_pos, fontsize=fontsize)
            # Print tau
            if tau_pos is not None and 'tau' in data:
                txt = 'tau={:0.2f}'.format(data['tau'])
                ax_txt_corner(ax, txt, tau_pos, fontsize=fontsize)
    return fig

if __name__ == '__main__':
    import argparse
    tinit = time.time()
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler)

    parser = argparse.ArgumentParser(description='wave inv')
    parser.add_argument('--verbose', action='store_true', default=False)
    parser.add_argument('--outputdir', default=OUTPUTDIR, help='folder where the data will be written [optional]',
                        required=False)
    parser.add_argument('--safe', required=True, help='SAFE to process', type=str)

    #parser.add_argument('--redo', action='store_true', default=False, help='redo existing files nc')
    #parser.add_argument('--dev', help='ca va plus vite',action='store_true',default=False)
    args = parser.parse_args()
    fmt = '%(asctime)s %(levelname)s %(filename)s(%(lineno)d) %(message)s'
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG, format=fmt,
                            datefmt='%d/%m/%Y %H:%M:%S')
    else:
        logging.basicConfig(level=logging.INFO, format=fmt,
                            datefmt='%d/%m/%Y %H:%M:%S')
    tops_osw_run(args.safe,output_dir=args.outputdir)
    logging.info('done in %1.3f min',(time.time() - tinit) / 60.)
    logging.info('peak memory usage: %s Mbytes',resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024.)
