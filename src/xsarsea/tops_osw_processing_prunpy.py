#!/home1/datawork/agrouaze/conda_envs2/envs/py2.7_cwave/bin/python
# coding: utf-8
"""
"""
import sys
print(sys.executable)
import subprocess
import os
import logging
import sys
# sys.path.append('/home1/datahome/agrouaze/git/mpc/data_collect')
# import get_path_from_base_SAFE
sys.path.append('/home1/datahome/agrouaze/git/sar_tops_osw/sar_tops_osw/processing')
from input_output_prun import dict_input_output
if __name__ == '__main__':
    root = logging.getLogger ()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler (handler)
    import argparse
    parser = argparse.ArgumentParser (description='start prun')
    parser.add_argument ('--verbose',action='store_true',default=False)
    args = parser.parse_args ()
    if args.verbose:
        logging.basicConfig (level=logging.DEBUG,format='%(asctime)s %(levelname)-5s %(message)s',
                             datefmt='%d/%m/%Y %H:%M:%S')
    else:
        logging.basicConfig (level=logging.INFO,format='%(asctime)s %(levelname)-5s %(message)s',
                             datefmt='%d/%m/%Y %H:%M:%S')
    prunexe = '/appli/prun/bin/prun'
    # listing = '/home1/datawork/agrouaze/data/sentinel1/inventaires/listing_medicanes_2021_IW_SLC.txt' # fait a la main via peps
    # listing_fullpath = '/home1/datawork/agrouaze/data/sentinel1/inventaires/listing_medicanes_2021_IW_SLC_fp.txt'
    # listing_fullpath = '/home1/datawork/agrouaze/data/sentinel1/inventaires/surigae_iw_slc_2021_april.txt' # fait a la main
    # listing_fullpath = '/home1/datawork/agrouaze/data/sentinel1/inventaires/listing_IW_SLC_bouee_lion.txt'
    listing_fullpath = '/home1/scratch/agrouaze/xsar_xspectra_iw_slc_processing.txt'
    fid = open(listing_fullpath,'w')
    cpt = 0
    if True:  # specific case
        dict_input_output2 = {}
        dict_input_output2['medicane_apollo'] = dict_input_output['medicane_apollo']
    else:
        dict_input_output2 = dict_input_output
    for kk in dict_input_output2:
        fud = open(dict_input_output2[kk][0])
        input_safes = fud.readlines()
        fud.close()
        for ss in input_safes:
            stence = '%s %s \n'%(ss.replace('\n',''),dict_input_output2[kk][1])
            fid.write(stence)
            logging.info('%s',stence)
            cpt += 1
    fid.close()
    logging.info('nb processing : %s',cpt)
    # call prun
    opts = ' --name xsarseaTOPSsp --split-max-lines=1 --background -e '
    listing_content = []

    logging.info('listing written ; %s',listing_fullpath)
    #pbs = '/home1/datahome/agrouaze/sources/git/sar_tops_osw/sar_tops_osw/processing/tops_osw_processing.pbs'
    pbs = '/home1/datahome/agrouaze/git/xsarseafork/src/xsarsea/tops_osw_processing.pbs'
    cmd = prunexe+opts+pbs+' '+listing_fullpath
    logging.info('cmd to cast = %s',cmd)
    st = subprocess.check_call(cmd,shell=True)
    logging.info('status cmd = %s',st)
